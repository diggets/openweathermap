(*
 *  Copyright 2016 Eric Berger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *)

unit OpenWeatherMapTypes;

interface

uses
  System.SysUtils,
  System.JSON,
  System.Rtti,
  System.TypInfo,
  OpenWeatherMapInterfaces;

type
  TWeatherObject      = class;
  TWeatherObjectClass = class of TWeatherObject;

  TWeatherObject = class(TInterfacedObject,
                         IWeatherObject)
    protected
      fOrigin : Boolean;
      fData   : TJSONObject;

      class function getRttiValue(const pJson : TJSONValue) : TValue;
    public
      property origin : Boolean read fOrigin;

      constructor Create(); virtual;
      destructor  Destroy(); override;

      function ToString() : String; override;

      class function fromJSON(const pData : TJSONObject) : IWeatherObject; overload;
      class function fromJSON(const pData : String) : IWeatherObject; overload;

      function  getJSONProperty(const pName : String) : TValue; virtual;
      function  getListProperty(const pName : String;
                                const pClass : TWeatherObjectClass) : TWeatherObjectList;
      function  getIntfProperty(const pName : String;
                                const pClass : TWeatherObjectClass) : IWeatherObject;
      procedure setJSONProperty(const pName : String;
                                const pValue : TValue); virtual;
  end;

  TWeatherError = class(TWeatherObject,
                        IWeatherError)
    protected
      function  getCod() : String;
      procedure setCod(const pValue : String);
      function  getMessage() : String;
      procedure setMessage(const pValue : String);

    public
      property cod     : String read getCod     write setCod;
      property message : String read getMessage write setMessage;

      class function isErrorObject(const pJSON : TJSONValue) : Boolean;
  end;

  TWeatherCoord = class(TWeatherObject,
                        IWeatherCoord)
    protected
      function  getLon() : Double;
      procedure setLon(const pValue : Double);
      function  getLat() : Double;
      procedure setLat(const pValue : Double);

    public
      property lon : Double read getLon write setLon;
      property lat : Double read getLat write setLat;
  end;

  TWeatherInfo = class(TWeatherObject,
                       IWeatherInfo)
    protected
      function  getId() : Integer;
      procedure setId(const pValue : Integer);
      function  getMain() : String;
      procedure setMain(const pValue : String);
      function  getDescription() : String;
      procedure setDescription(const pValue : String);
      function  getIcon() : String;
      procedure setIcon(const pValue : String);

    public
      property id          : Integer read getId          write setId;
      property main        : String  read getMain        write setMain;
      property description : String  read getDescription write setDescription;
      property icon        : String  read getIcon        write setIcon;
  end;

  TWeatherMain = class(TWeatherObject,
                       IWeatherMain)
    protected
      function  getTemp() : Double;
      procedure setTemp(const pValue : Double);
      function  getPressure() : Double;
      procedure setPressure(const pValue : Double);
      function  getHumidity() : Double;
      procedure setHumidity(const pValue : Double);
      function  getTempMin() : Double;
      procedure setTempMin(const pValue : Double);
      function  getTempMax() : Double;
      procedure setTempMax(const pValue : Double);
      function  getSeaLevel() : Double;
      procedure setSeaLevel(const pValue : Double);
      function  getGrndLevel() : Double;
      procedure setGrndLevel(const pValue : Double);
      function  getTempKF() : Double;
      procedure setTempKF(const pValue : Double);

    public
      property temp       : Double read getTemp       write setTemp;
      property pressure   : Double read getPressure   write setPressure;
      property humidity   : Double read getHumidity   write setHumidity;
      property temp_min   : Double read getTempMin    write setTempMin;
      property temp_max   : Double read getTempMax    write setTempMax;
      property sea_level  : Double read getSeaLevel   write setSeaLevel;
      property grnd_level : Double read getGrndLevel  write setGrndLevel;
      property temp_kf    : Double read getTempKF     write setTempKF;
  end;

  TWeatherWind = class(TWeatherObject,
                       IWeatherWind)
    protected
      function  getSpeed() : Double;
      procedure setSpeed(const pValue : Double);
      function  getDeg() : Double;
      procedure setDeg(const pValue : Double);

    public
      property speed : Double read getSpeed write setSpeed;
      property deg   : Double read getDeg   write setDeg;
  end;

  TWeatherRain = class(TWeatherObject,
                       IWeatherRain)
    protected
      function  getThreeHours() : Double;
      procedure setThreeHours(const pValue : Double);

    public
      property three_hours : Double read getThreeHours write setThreeHours;
  end;

  TWeatherSnow = class(TWeatherObject,
                       IWeatherSnow)
    protected
    public
  end;

  TWeatherClouds = class(TWeatherObject,
                         IWeatherClouds)
    protected
      function  getAll() : Integer;
      procedure setAll(const pValue : Integer);

    public
      property all : Integer read getAll write setAll;
  end;

  TWeatherCity = class(TWeatherObject, IWeatherCity)
    protected
      fCoord : IWeatherCoord;
      fSys   : IWeatherSys;

      function  getId() : Integer;
      procedure setId(const pValue : Integer);
      function  getName() : String;
      procedure setName(const pValue : String);
      function  getCoord() : IWeatherCoord;
      procedure setCoord(const pValue : IWeatherCoord);
      function  getCountry() : String;
      procedure setCountry(const pValue : String);
      function  getPopulation() : Integer;
      procedure setPopulation(const pValue : Integer);
      function  getSys() : IWeatherSys;
      procedure setSys(const pValue : IWeatherSys);

    public
      property id         : Integer       read getId         write setId;
      property name       : String        read getName       write setName;
      property coord      : IWeatherCoord read getCoord      write setCoord;
      property country    : String        read getCountry    write setCountry;
      property population : Integer       read getPopulation write setPopulation;
      property sys        : IWeatherSys   read getSys        write setSys;
  end;

  TWeatherSys = class(TWeatherObject,
                      IWeatherSys)
    protected
      function  getType() : Integer;
      procedure setType(const pValue : Integer);
      function  getId() : Integer;
      procedure setId(const pValue : Integer);
      function  getMessage() : Double;
      procedure setMessage(const pValue : Double);
      function  getCountry() : String;
      procedure setCountry(const pValue : String);
      function  getSunrise() : Cardinal;
      procedure setSunrise(const pValue : Cardinal);
      function  getSunset() : Cardinal;
      procedure setSunset(const pValue : Cardinal);
      function  getPod() : String;
      procedure setPod(const pValue : String);
      function  getPopulation() : Integer;
      procedure setPopulation(const pValue : Integer);

    public
      property _type      : Integer  read getType       write setType;
      property id         : Integer  read getId         write setId;
      property message    : Double   read getMessage    write setMessage;
      property country    : String   read getCountry    write setCountry;
      property sunrise    : Cardinal read getSunrise    write setSunrise;
      property sunset     : Cardinal read getSunset     write setSunset;
      property pod        : String   read getPod        write setPod;
      property population : Integer  read getPopulation write setPopulation;
  end;

  TCustomWeather = class(TWeatherObject,
                         ICustomWeather)
    protected
      fMain    : IWeatherMain;
      fWeather : TWeatherObjectList;
      fWind    : IWeatherWind;
      fClouds  : IWeatherClouds;
      fSys     : IWeatherSys;

      function  getMain() : IWeatherMain;
      procedure setMain(const pValue : IWeatherMain);
      function  getWeather() : TWeatherObjectList;
      procedure setWeather(const pValue : TWeatherObjectList);
      function  getWind() : IWeatherWind;
      procedure setWind(const pValue : IWeatherWind);
      function  getClouds() : IWeatherClouds;
      procedure setClouds(const pValue : IWeatherClouds);
      function  getDt() : Cardinal;
      procedure setDt(const pValue : Cardinal);
      function  getSys() : IWeatherSys;
      procedure setSys(const pValue : IWeatherSys);

    public
      property main       : IWeatherMain   		   read getMain        write setMain;
      property weather    : TWeatherObjectList read getWeather     write setWeather;
      property wind       : IWeatherWind   		   read getWind        write setWind;
      property clouds     : IWeatherClouds 		   read getClouds      write setClouds;
      property dt         : Cardinal          	 read getDt          write setDt;
      property sys        : IWeatherSys    		   read getSys         write setSys;

      constructor Create(); override;
      destructor  Destroy(); override;

      function getTemperature() : Double; virtual;
      function getTimestamp() : TDateTime; virtual;
      function getIconFile() : String; virtual;
  end;

  TCurrentWeather = class(TCustomWeather,
                          ICurrentWeather)
    protected
      fCoord : IWeatherCoord;

      function  getCoord() : IWeatherCoord;
      procedure setCoord(const pValue : IWeatherCoord);
      function  getBase() : String;
      procedure setBase(const pValue : String);
      function  getVisibility() : Integer;
      procedure setVisibility(const pValue : Integer);
      function  getId() : Integer;
      procedure setId(const pValue : Integer);
      function  getName() : String;
      procedure setName(const pValue : String);
      function  getCod() : Integer;
      procedure setCod(const pValue : Integer);

    public
      property coord      : IWeatherCoord  		   read getCoord       write setCoord;
      property base       : String            	 read getBase        write setBase;
      property visibility : Integer           	 read getVisibility  write setVisibility;
      property id         : Integer           	 read getId          write setId;
      property name       : String            	 read getName        write setName;
      property cod        : Integer           	 read getCod         write setCod;

      function getLocationName() : String; virtual;
  end;

  T5DayForecastWeather = class(TCustomWeather,
                               I5DayForecastWeather)
    protected
      fRain : IWeatherRain;
      fSnow : IWeatherSnow;

      function  getRain() : IWeatherRain;
      procedure setRain(const pValue : IWeatherRain);
      function  getSnow() : IWeatherSnow;
      procedure setSnow(const pValue : IWeatherSnow);
      function  getDtTxt() : String;
      procedure setDtTxt(const pValue : String);


    public
      property rain   : IWeatherRain read getRain  write setRain;
      property snow   : IWeatherSnow read getSnow  write setSnow;
      property dt_txt : String       read getDtTxt write setDtTxt;
  end;

  TWeather5DayForecast = class(TWeatherObject,
                               IWeather5DayForecast)
    protected
      fCity : IWeatherCity;
      fList : TWeatherObjectList;

      function  getCity() : IWeatherCity;
      procedure setCity(const pValue : IWeatherCity);
      function  getMessage() : Double;
      procedure setMessage(const pValue : Double);
      function  getCod() : String;
      procedure setCod(const pValue : String);
      function  getCnt() : Integer;
      procedure setCnt(const pValue : Integer);
      function  getList() : TWeatherObjectList;
      procedure setList(const pValue : TWeatherObjectList);

    public
      property city    : IWeatherCity         read getCity    write setCity;
      property message : Double               read getMessage write setMessage;
      property cod     : String               read getCod     write setCod;
      property cnt     : Integer              read getCnt     write setCnt;
      property list    : TWeatherObjectList read getList    write setList;

      constructor Create(); override;
      destructor  Destroy(); override;

      function getMinimum() : I5DayForecastWeather;
      function getMinimumTemperature() : Double;
      function getMaximum() : I5DayForecastWeather;
      function getMaximumTemperature() : Double;
  end;

  TWeatherStationInfo = class(TWeatherObject,
                              IWeatherStationInfo)
    protected
      fCoord : IWeatherCoord;

      function  getName() : String;
      procedure setName(const pValue : String);
      function  getType() : Integer;
      procedure setType(const pValue : Integer);
      function  getStatus() : Integer;
      procedure setStatus(const pValue : Integer);
      function  getId() : Integer;
      procedure setId(const pValue : Integer);
      function  getCoord() : IWeatherCoord;
      procedure setCoord(const pValue : IWeatherCoord);

    public
      property name   : String        read getName    write setName;
      property _type  : Integer       read getType    write setType;
      property status : Integer       read getStatus  write setStatus;
      property id     : Integer       read getId      write setId;
      property coord  : IWeatherCoord read getCoord   write setCoord;
  end;

  TWeatherStation = class(TWeatherObject,
                          IWeatherStation)
    protected
      fStation : IWeatherStationInfo;
      fLast    : ICurrentWeather;

      function  getStation() : IWeatherStationInfo;
      procedure setStation(const pValue : IWeatherStationInfo);
      function  getDistance() : Double;
      procedure setDistance(const pValue : Double);
      function  getLast() : ICurrentWeather;
      procedure setLast(const pValue : ICurrentWeather);

    public
      property station  : IWeatherStationInfo read getStation   write setStation;
      property distance : Double              read getDistance  write setDistance;
      property last     : ICurrentWeather     read getLast      write setLast;
  end;

implementation

constructor TWeatherObject.Create();
begin
  inherited Create();
end;

destructor TWeatherObject.Destroy();
begin
  try
    if Self.fOrigin then
      freeAndNil(fData);
  except
    on e:exception do
    begin
      raise Exception.Create('TWeatherObject.data assigned, but already destroyed');
    end;
  end;

  inherited Destroy();
end;

class function TWeatherObject.getRttiValue(const pJson : TJSONValue) : TValue;
var lList      : TWeatherObjectList;
    lStrVal    : String;
    lValue     : TValue;
    lEnum      : TJSONArrayEnumerator;
begin
  if (pJSON is TJSONTrue) then
    result := TValue.From<Boolean>(true)
  else if (pJSON is TJSONFalse) then
    result := TValue.From<Boolean>(false)
  else if (pJSON is TJSONNumber) then
  begin
    lStrVal := TJSONNumber(pJSON).ToString();
    if lStrVal.Contains('.') then
      result := TValue.From<Double>(TJSONNumber(pJSON).AsDouble)
    else
      result := TValue.From<Integer>(TJSONNumber(pJSON).AsInt);
  end
  else if (pJSON is TJSONArray) then
  begin
    lList := TWeatherObjectList.Create();
    try
      lEnum := TJSONArray(pJSON).GetEnumerator();
      try
        while lEnum.MoveNext() do
        begin
          lValue := getRttiValue(lEnum.Current);
          if not (lValue.Kind = tkInterface) then
          begin

          end;
        end;
      finally
        lEnum.free();
      end;
    finally
      result := TValue.From<TWeatherObjectList>(lList);
    end;
  end
  else if (pJSON is TJSONObject) then
  begin
    result := TValue.Empty;
  end
  else if (pJSON is TJSONString) then
  begin
    result := TValue.From<String>(pJSON.ToString());
  end
  else
  begin
    result := TValue.Empty;
  end;
end;

class function TWeatherObject.fromJSON(const pData : TJSONObject) : IWeatherObject;
var lInst : TWeatherObject;
begin
  if not TWeatherError.isErrorObject(pData) then
    lInst := Self.Create()
  else
    lInst := TWeatherError.Create();

  lInst.fOrigin := true;
  lInst.fData   := TJSONObject(pData);

  result := lInst as IWeatherObject;
end;

class function TWeatherObject.fromJSON(const pData : String) : IWeatherObject;
var lJSON : TJSONValue;
begin
  result := nil;

  lJSON := TJSONObject.ParseJSONValue(pData);
  try
    if (lJSON is TJSONObject) then
    begin
      result := Self.fromJSON(TJSONObject(lJSON));
    end;
  finally
//    if assigned(lJson) then
//      freeAndNil(lJson);
  end;
end;

function TWeatherObject.getJSONProperty(const pName : String) : TValue;
var lJsonVal : TJSONValue;
begin
  lJsonVal := fData.GetValue(pName);
  result   := TWeatherObject.getRttiValue(lJsonVal);
end;

function TWeatherObject.getListProperty(const pName : String;
                                          const pClass : TWeatherObjectClass) : TWeatherObjectList;
var lJsonVal : TJSONValue;
    lEnum    : TJSONArrayEnumerator;
    lInst    : TWeatherObject;
    lIntf    : IWeatherObject;
begin
  result := TWeatherObjectList.Create();
  lJsonVal := fData.GetValue(pName);
  if (lJsonVal is TJSONArray) then
  begin
    lEnum := TJSONArray(lJSONVal).GetEnumerator();
    try
      while lEnum.MoveNext() do
      begin
        if (lEnum.Current is TJSONObject) then
        begin
          lInst       := pClass.Create();
          lInst.fData := TJSONObject(lEnum.Current);
          lIntf       := lInst as IWeatherObject;
          result.add(lIntf as IWeatherObject);
        end;
      end;
    finally
      lEnum.free();
    end;
  end;
end;

function TWeatherObject.getIntfProperty(const pName : String;
                                        const pClass : TWeatherObjectClass) : IWeatherObject;
var lJsonVal : TJSONValue;
    lInst    : TWeatherObject;
begin
  result   := nil;
  lJsonVal := fData.GetValue(pName);
  if (lJsonVal is TJSONObject) then
  begin
    lInst       := pClass.Create();
    lInst.fData := TJSONObject(lJsonVal);
    result      := lInst as IWeatherObject;
  end;
end;

procedure TWeatherObject.setJSONProperty(const pName : String;
                                         const pValue : TValue);
										
	function toJSON(const pValue : TValue) : TJSONValue;
	begin
	  case pValue.kind of
		tkInteger 	  : result := TJSONNumber.Create(pValue.AsInteger());
		tkInt64   	  : result := TJSONNumber.Create(pValue.AsInt64());
		tkFloat	  	  : result := TJSONNumber.Create(pValue.AsExtended());
		tkChar,
		tkWChar,
		tkString,
		tkAnsiString,
		tkWideString,
		tkUString	  : result := TJSONNumber.Create(pValue.AsString());
		tkEnumeration : if (pValue.TypeInfo^.Name = 'Boolean') then
                    begin
                      if pValue.AsBoolean() then result := TJSONTrue.Create()
                                else result := TJSONFalse.Create();
                    end
                    else result := TJSONNull.Create();
						
		else result := TJSONNull.Create();
	  end;
	end;

var lJsonVal : TJSONValue;
begin
  if assigned(fData.GetValue(pName)) then
	  fData.RemovePair(pName);
	
  lJsonVal := toJSON(pValue);
  fData.AddPair(pName, lJsonVal);
end;

function TWeatherObject.ToString() : String;
begin
  if assigned(fData) then result := fData.ToJSON()
                     else result := '{}';
end;










function TWeatherError.getCod() : String;
begin
  result := getJSONProperty('cod').AsString();
end;

procedure TWeatherError.setCod(const pValue : String);
begin
  setJSONProperty('cod', TValue.From<String>(pValue));
end;

function TWeatherError.getMessage() : String;
begin
  result := getJSONProperty('message').AsString();
end;

procedure TWeatherError.setMessage(const pValue : String);
begin
  setJSONProperty('message', TValue.From<String>(pValue));
end;

class function TWeatherError.isErrorObject(const pJSON : TJSONValue) : Boolean;
var lVal : TJSONValue;
begin
  result := false;
  if (pJSON is TJSONObject) and (TJSONObject(pJSON).Count = 2) then
  begin
    lVal := TJSONObject(pJSON).GetValue('cod');
    if assigned(lVal) then
    begin
      lVal   := TJSONObject(pJSON).GetValue('message');
      result := assigned(lVal);
    end;
  end;
end;














function TWeatherCoord.getLon() : Double;
begin
  result := getJSONProperty('lon').AsExtended();
end;

procedure TWeatherCoord.setLon(const pValue : Double);
begin
  setJSONProperty('lon', TValue.From<Double>(pValue));
end;

function TWeatherCoord.getLat() : Double;
begin
  result := getJSONProperty('lat').AsExtended();
end;

procedure TWeatherCoord.setLat(const pValue : Double);
begin
  setJSONProperty('lat', TValue.From<Double>(pValue));
end;





function TWeatherInfo.getId() : Integer;
begin
  result := getJSONProperty('id').AsInteger();
end;

procedure TWeatherInfo.setId(const pValue : Integer);
begin
  setJSONProperty('id', TValue.From<Integer>(pValue));
end;

function TWeatherInfo.getMain() : String;
begin
  result := getJSONProperty('main').AsString();
end;

procedure TWeatherInfo.setMain(const pValue : String);
begin
  setJSONProperty('main', TValue.From<String>(pValue));
end;

function TWeatherInfo.getDescription() : String;
begin
  result := getJSONProperty('description').AsString();
end;

procedure TWeatherInfo.setDescription(const pValue : String);
begin
  setJSONProperty('description', TValue.From<String>(pValue));
end;

function TWeatherInfo.getIcon() : String;
begin
  result := getJSONProperty('icon').AsString();
end;

procedure TWeatherInfo.setIcon(const pValue : String);
begin
  setJSONProperty('icon', TValue.From<String>(pValue));
end;






function TWeatherMain.getTemp() : Double;
begin
  result := getJSONProperty('temp').AsExtended();
end;

procedure TWeatherMain.setTemp(const pValue : Double);
begin
  setJSONProperty('temp', TValue.From<Double>(pValue));
end;

function TWeatherMain.getPressure() : Double;
begin
  result := getJSONProperty('pressure').AsExtended();
end;

procedure TWeatherMain.setPressure(const pValue : Double);
begin
  setJSONProperty('pressure', TValue.From<Double>(pValue));
end;

function TWeatherMain.getHumidity() : Double;
begin
  result := getJSONProperty('humidity').AsExtended();
end;

procedure TWeatherMain.setHumidity(const pValue : Double);
begin
  setJSONProperty('humidity', TValue.From<Double>(pValue));
end;

function TWeatherMain.getTempMin() : Double;
begin
  result := getJSONProperty('temp_min').AsExtended();
end;

procedure TWeatherMain.setTempMin(const pValue : Double);
begin
  setJSONProperty('temp_min', TValue.From<Double>(pValue));
end;

function TWeatherMain.getTempMax() : Double;
begin
  result := getJSONProperty('temp_max').AsExtended();
end;

procedure TWeatherMain.setTempMax(const pValue : Double);
begin
  setJSONProperty('temp_max', TValue.From<Double>(pValue));
end;

function TWeatherMain.getSeaLevel() : Double;
begin
  result := getJSONProperty('sea_level').AsExtended();
end;

procedure TWeatherMain.setSeaLevel(const pValue : Double);
begin
  setJSONProperty('sea_level', TValue.From<Double>(pValue));
end;

function TWeatherMain.getGrndLevel() : Double;
begin
  result := getJSONProperty('grnd_level').AsExtended();
end;

procedure TWeatherMain.setGrndLevel(const pValue : Double);
begin
  setJSONProperty('grnd_level', TValue.From<Double>(pValue));
end;

function TWeatherMain.getTempKF() : Double;
begin
  result := getJSONProperty('temp_kf').AsExtended();
end;

procedure TWeatherMain.setTempKF(const pValue : Double);
begin
  setJSONProperty('temp_kf', TValue.From<Double>(pValue));
end;










function TWeatherWind.getSpeed() : Double;
begin
  result := getJSONProperty('speed').AsExtended();
end;

procedure TWeatherWind.setSpeed(const pValue : Double);
begin
  setJSONProperty('speed', TValue.From<Double>(pValue));
end;

function TWeatherWind.getDeg() : Double;
begin
  result := getJSONProperty('deg').AsExtended();
end;

procedure TWeatherWind.setDeg(const pValue : Double);
begin
  setJSONProperty('deg', TValue.From<Double>(pValue));
end;








function TWeatherRain.getThreeHours() : Double;
begin
  result := getJSONProperty('3h').AsExtended();
end;

procedure TWeatherRain.setThreeHours(const pValue : Double);
begin
  setJSONProperty('3h', TValue.From<Double>(pValue));
end;








function TWeatherClouds.getAll() : Integer;
begin
  result := getJSONProperty('all').AsInteger();
end;

procedure TWeatherClouds.setAll(const pValue : Integer);
begin
  setJSONProperty('all', TValue.From<Integer>(pValue));
end;








function TWeatherCity.getId() : Integer;
begin
  result := getJSONProperty('id').AsInteger();
end;

procedure TWeatherCity.setId(const pValue : Integer);
begin
  setJSONProperty('id', TValue.From<Integer>(pValue));
end;

function TWeatherCity.getName() : String;
begin
  result := getJSONProperty('name').AsString();
end;

procedure TWeatherCity.setName(const pValue : String);
begin
  setJSONProperty('name', TValue.From<String>(pValue));
end;

function TWeatherCity.getCoord() : IWeatherCoord;
begin
  if not assigned(fCoord) then
    fCoord := getIntfProperty('coord', TWeatherCoord) as IWeatherCoord;

  result := fCoord;
end;

procedure TWeatherCity.setCoord(const pValue : IWeatherCoord);
begin
  fCoord := pValue;
end;

function TWeatherCity.getCountry() : String;
begin
  result := getJSONProperty('country').AsString();
end;

procedure TWeatherCity.setCountry(const pValue : String);
begin
  setJSONProperty('country', TValue.From<String>(pValue));
end;

function TWeatherCity.getPopulation() : Integer;
begin
  result := getJSONProperty('population').AsInteger();
end;

procedure TWeatherCity.setPopulation(const pValue : Integer);
begin
  setJSONProperty('population', TValue.From<Integer>(pValue));
end;

function TWeatherCity.getSys() : IWeatherSys;
begin
  if not assigned(fSys) then
    fSys := getIntfProperty('sys', TWeatherSys) as IWeatherSys;

  result := fSys;
end;

procedure TWeatherCity.setSys(const pValue : IWeatherSys);
begin
  fSys := pValue;
end;










function TWeatherSys.getType() : Integer;
begin
  result := getJSONProperty('type').AsInteger();
end;

procedure TWeatherSys.setType(const pValue : Integer);
begin
  setJSONProperty('type', TValue.From<Integer>(pValue));
end;

function TWeatherSys.getId() : Integer;
begin
  result := getJSONProperty('id').AsInteger();
end;

procedure TWeatherSys.setId(const pValue : Integer);
begin
  setJSONProperty('id', TValue.From<Integer>(pValue));
end;

function TWeatherSys.getMessage() : Double;
begin
  result := getJSONProperty('message').AsExtended();
end;

procedure TWeatherSys.setMessage(const pValue : Double);
begin
  setJSONProperty('message', TValue.From<Double>(pValue));
end;

function TWeatherSys.getCountry() : String;
begin
  result := getJSONProperty('country').AsString();
end;

procedure TWeatherSys.setCountry(const pValue : String);
begin
  setJSONProperty('country', TValue.From<String>(pValue));
end;

function TWeatherSys.getSunrise() : Cardinal;
begin
  result := Cardinal(getJSONProperty('sunrise').AsInteger());
end;

procedure TWeatherSys.setSunrise(const pValue : Cardinal);
begin
  setJSONProperty('sunrise', TValue.From<Integer>(pValue));
end;

function TWeatherSys.getSunset() : Cardinal;
begin
  result := Cardinal(getJSONProperty('sunset').AsInteger());
end;

procedure TWeatherSys.setSunset(const pValue : Cardinal);
begin
  setJSONProperty('sunset', TValue.From<Integer>(pValue));
end;

function TWeatherSys.getPod() : String;
begin
  result := getJSONProperty('pod').AsString();
end;

procedure TWeatherSys.setPod(const pValue : String);
begin
  setJSONProperty('pod', TValue.From<String>(pValue));
end;

function TWeatherSys.getPopulation() : Integer;
begin
  result := getJSONProperty('population').AsInteger();
end;

procedure TWeatherSys.setPopulation(const pValue : Integer);
begin
  setJSONProperty('population', TValue.From<Integer>(pValue));
end;






constructor TCustomWeather.Create();
begin
  inherited Create();
end;

destructor TCustomWeather.Destroy();
begin
  freeAndNil(fWeather);

  inherited Destroy();
end;

function TCustomWeather.getWeather() : TWeatherObjectList;
begin
  if not assigned(fWeather) then
    fWeather := getListProperty('weather', TWeatherInfo);

  result := fWeather;
end;

procedure TCustomWeather.setWeather(const pValue : TWeatherObjectList);
begin
  fWeather := pValue;
end;

function TCustomWeather.getMain() : IWeatherMain;
begin
  if not assigned(fMain) then
    fMain := getIntfProperty('main', TWeatherMain) as IWeatherMain;

  result := fMain;
end;

procedure TCustomWeather.setMain(const pValue : IWeatherMain);
begin
  fMain := pValue;
end;

function TCustomWeather.getWind() : IWeatherWind;
begin
  if not assigned(fWind) then
    fWind := getIntfProperty('wind', TWeatherWind) as IWeatherWind;

  result := fWind;
end;

procedure TCustomWeather.setWind(const pValue : IWeatherWind);
begin
  fWind := pValue;
end;

function TCustomWeather.getClouds() : IWeatherClouds;
begin
  if not assigned(fClouds) then
    fClouds := getIntfProperty('clouds', TWeatherClouds) as IWeatherClouds;

  result := fClouds;
end;

procedure TCustomWeather.setClouds(const pValue : IWeatherClouds);
begin
  fClouds := pValue;
end;

function TCustomWeather.getDt() : Cardinal;
begin
  result := Cardinal(getJSONProperty('dt').AsInteger());
end;

procedure TCustomWeather.setDt(const pValue : Cardinal);
begin
  setJSONProperty('dt', TValue.From<Cardinal>(pValue));
end;

function TCustomWeather.getSys() : IWeatherSys;
begin
  if not assigned(fSys) then
    fSys := getIntfProperty('sys', TWeatherSys) as IWeatherSys;

  result := fSys;
end;

procedure TCustomWeather.setSys(const pValue : IWeatherSys);
begin
  fSys := pValue;
end;

function TCustomWeather.getTemperature() : Double;
begin
  if assigned(Self.main) then
    result := Self.main.temp
  else
    result := 0;
end;

function TCustomWeather.getTimestamp() : TDateTime;
begin
  Result := ((Self.dt + 7200) / 86400) + 25569;
end;

function TCustomWeather.getIconFile() : String;
var lInfo : IWeatherInfo;
begin
  lInfo := Self.weather.first() as IWeatherInfo;
  case lInfo.id of
    // Thunderstorm
    200	: result := '11d.png'; // thunderstorm with light rain	[[file:11d.png]]
    201	: result := '11d.png'; // thunderstorm with rain	[[file:11d.png]]
    202	: result := '11d.png'; // thunderstorm with heavy rain	[[file:11d.png]]
    210	: result := '11d.png'; // light thunderstorm	[[file:11d.png]]
    211	: result := '11d.png'; // thunderstorm	[[file:11d.png]]
    212	: result := '11d.png'; // heavy thunderstorm	[[file:11d.png]]
    221	: result := '11d.png'; // ragged thunderstorm	[[file:11d.png]]
    230	: result := '11d.png'; // thunderstorm with light drizzle	[[file:11d.png]]
    231	: result := '11d.png'; // thunderstorm with drizzle	[[file:11d.png]]
    232	: result := '11d.png'; // thunderstorm with heavy drizzle	[[file:11d.png]]

    // Drizzle
    300	: result := '09d.png'; // light intensity drizzle	[[file:09d.png]]
    301	: result := '09d.png'; // drizzle	[[file:09d.png]]
    302	: result := '09d.png'; // heavy intensity drizzle	[[file:09d.png]]
    310	: result := '09d.png'; // light intensity drizzle rain	[[file:09d.png]]
    311	: result := '09d.png'; // drizzle rain	[[file:09d.png]]
    312	: result := '09d.png'; // heavy intensity drizzle rain	[[file:09d.png]]
    313	: result := '09d.png'; // shower rain and drizzle	[[file:09d.png]]
    314	: result := '09d.png'; // heavy shower rain and drizzle	[[file:09d.png]]
    321	: result := '09d.png'; // shower drizzle	[[file:09d.png]]

    // Rain
    500	: result := '10d.png'; // light rain	[[file:10d.png]]
    501	: result := '10d.png'; // moderate rain	[[file:10d.png]]
    502	: result := '10d.png'; // heavy intensity rain	[[file:10d.png]]
    503	: result := '10d.png'; // very heavy rain	[[file:10d.png]]
    504	: result := '10d.png'; // extreme rain	[[file:10d.png]]
    511	: result := '13d.png'; // freezing rain	[[file:13d.png]]
    520	: result := '09d.png'; // light intensity shower rain	[[file:09d.png]]
    521	: result := '09d.png'; // shower rain	[[file:09d.png]]
    522	: result := '09d.png'; // heavy intensity shower rain	[[file:09d.png]]
    531	: result := '09d.png'; // ragged shower rain	[[file:09d.png]]

    // Snow
    600	: result := '13d.png'; // light snow	[[file:13d.png]]
    601	: result := '13d.png'; // snow	[[file:13d.png]]
    602	: result := '13d.png'; // heavy snow	[[file:13d.png]]
    611	: result := '13d.png'; // sleet	[[file:13d.png]]
    612	: result := '13d.png'; // shower sleet	[[file:13d.png]]
    615	: result := '13d.png'; // light rain and snow	[[file:13d.png]]
    616	: result := '13d.png'; // rain and snow	[[file:13d.png]]
    620	: result := '13d.png'; // light shower snow	[[file:13d.png]]
    621	: result := '13d.png'; // shower snow	[[file:13d.png]]
    622	: result := '13d.png'; // heavy shower snow	[[file:13d.png]]

    // Atmosphere
    701	: result := '50d.png'; // mist	[[file:50d.png]]
    711	: result := '50d.png'; // smoke	[[file:50d.png]]
    721	: result := '50d.png'; // haze	[[file:50d.png]]
    731	: result := '50d.png'; // sand, dust whirls	[[file:50d.png]]
    741	: result := '50d.png'; // fog	[[file:50d.png]]
    751	: result := '50d.png'; // sand	[[file:50d.png]]
    761	: result := '50d.png'; // dust	[[file:50d.png]]
    762	: result := '50d.png'; // volcanic ash	[[file:50d.png]]
    771	: result := '50d.png'; // squalls	[[file:50d.png]]
    781	: result := '50d.png'; // tornado	[[file:50d.png]]

    // Clouds
    800	: result := '01d.png'; // clear sky	[[file:01d.png]] [[file:01n.png]]
    801	: result := '02d.png'; // few clouds	[[file:02d.png]] [[file:02n.png]]
    802	: result := '03d.png'; // scattered clouds	[[file:03d.png]] [[file:03d.png]]
    803	: result := '04d.png'; // broken clouds	[[file:04d.png]] [[file:03d.png]]
    804	: result := '04d.png'; // overcast clouds	[[file:04d.png]] [[file:04d.png]]

    // Extreme
    900	: result := 'default.png'; // tornado
    901	: result := 'default.png'; // tropical storm
    902	: result := 'default.png'; // hurricane
    903	: result := 'default.png'; // cold
    904	: result := 'default.png'; // hot
    905	: result := 'default.png'; // windy
    906	: result := 'default.png'; // hail

    // Additional
    951	: result := 'default.png'; // calm
    952	: result := 'default.png'; // light breeze
    953	: result := 'default.png'; // gentle breeze
    954	: result := 'default.png'; // moderate breeze
    955	: result := 'default.png'; // fresh breeze
    956	: result := 'default.png'; // strong breeze
    957	: result := 'default.png'; // high wind, near gale
    958	: result := 'default.png'; // gale
    959	: result := 'default.png'; // severe gale
    960	: result := 'default.png'; // storm
    961	: result := 'default.png'; // violent storm
    962	: result := 'default.png'; // hurricane

    else result := 'default.png';
  end;
end;












function TCurrentWeather.getCoord() : IWeatherCoord;
begin
  if not assigned(fCoord) then
    fCoord := getIntfProperty('coord', TWeatherCoord) as IWeatherCoord;

  result := fCoord;
end;

procedure TCurrentWeather.setCoord(const pValue : IWeatherCoord);
begin
  fCoord := pValue;
end;

function TCurrentWeather.getBase() : String;
begin
  result := getJSONProperty('base').AsString();
end;

procedure TCurrentWeather.setBase(const pValue : String);
begin
  setJSONProperty('base', TValue.From<String>(pValue));
end;

function TCurrentWeather.getVisibility() : Integer;
begin
  result := getJSONProperty('visibility').AsInteger();
end;

procedure TCurrentWeather.setVisibility(const pValue : Integer);
begin
  setJSONProperty('visibility', TValue.From<Integer>(pValue));
end;

function TCurrentWeather.getId() : Integer;
begin
  result := Cardinal(getJSONProperty('id').AsInteger());
end;

procedure TCurrentWeather.setId(const pValue : Integer);
begin
  setJSONProperty('id', TValue.From<Integer>(pValue));
end;

function TCurrentWeather.getName() : String;
begin
  result := getJSONProperty('name').AsString();
end;

procedure TCurrentWeather.setName(const pValue : String);
begin
  setJSONProperty('name', TValue.From<String>(pValue));
end;

function TCurrentWeather.getCod() : Integer;
begin
  result := Cardinal(getJSONProperty('cod').AsInteger());
end;

procedure TCurrentWeather.setCod(const pValue : Integer);
begin
  setJSONProperty('cod', TValue.From<Integer>(pValue));
end;

function TCurrentWeather.getLocationName() : String;
begin
  result := name.Replace('"', '', [rfReplaceAll]);
end;









{

  T5DayForecastWeather

}
function T5DayForecastWeather.getDtTxt() : String;
begin
  result := getJSONProperty('dt_txt').AsString();
end;

procedure T5DayForecastWeather.setDtTxt(const pValue : String);
begin
  setJSONProperty('dt_txt', TValue.From<String>(pValue));
end;

function T5DayForecastWeather.getRain() : IWeatherRain;
begin
  if not assigned(fRain) then
    fRain := getIntfProperty('rain', TWeatherRain) as IWeatherRain;

  result := fRain;
end;

procedure T5DayForecastWeather.setRain(const pValue : IWeatherRain);
begin
  fRain := pValue;
end;

function T5DayForecastWeather.getSnow() : IWeatherSnow;
begin
  if not assigned(fSnow) then
    fSnow := getIntfProperty('snow', TWeatherSnow) as IWeatherSnow;

  result := fSnow;
end;

procedure T5DayForecastWeather.setSnow(const pValue : IWeatherSnow);
begin
  fSnow := pValue;
end;







constructor TWeather5DayForecast.Create();
begin
  inherited Create();
end;

destructor TWeather5DayForecast.Destroy();
begin
  freeAndNil(fList);

  inherited Destroy();
end;

function TWeather5DayForecast.getCity() : IWeatherCity;
begin
  if not assigned(fCity) then
    fCity := getIntfProperty('city', TWeatherCity) as IWeatherCity;

  result := fCity;
end;

procedure TWeather5DayForecast.setCity(const pValue : IWeatherCity);
begin
  fCity := pValue;
end;

function TWeather5DayForecast.getCod() : String;
begin
  result := getJSONProperty('cod').AsString();
end;

procedure TWeather5DayForecast.setCod(const pValue : String);
begin
  setJSONProperty('cod', TValue.From<String>(pValue));
end;

function TWeather5DayForecast.getMessage() : Double;
begin
  result := getJSONProperty('message').AsExtended();
end;

procedure TWeather5DayForecast.setMessage(const pValue : Double);
begin
  setJSONProperty('message', TValue.From<Double>(pValue));
end;

function TWeather5DayForecast.getCnt() : Integer;
begin
  result := getJSONProperty('cnt').AsInteger();
end;

procedure TWeather5DayForecast.setCnt(const pValue : Integer);
begin
  setJSONProperty('cnt', TValue.From<Integer>(pValue));
end;

function TWeather5DayForecast.getList() : TWeatherObjectList;
begin
  if not assigned(fList) then
    fList := getListProperty('list', T5DayForecastWeather);

  result := fList;
end;

procedure TWeather5DayForecast.setList(const pValue : TWeatherObjectList);
begin
  fList := pValue;
end;

function TWeather5DayForecast.getMinimum() : I5DayForecastWeather;
var lEnum    : TWeatherObjectList.TEnumerator;
    lWeather : I5DayForecastWeather;
    lIsSet   : Boolean;
    lCurrent,
    lTemp    : Double;
begin
  lTemp  := 0;
  result := nil;

  lEnum := list.GetEnumerator();
  try
    lIsSet := false;
    while lEnum.MoveNext() do
    begin
      lWeather := lEnum.Current as I5DayForecastWeather;
      lCurrent := lWeather.getTemperature();
      if lIsSet then
      begin
        if (lCurrent < lTemp) then
        begin
          result := lWeather;
          lTemp  := lCurrent;
        end;
      end
      else
      begin
        result := lWeather;
        lTemp  := lCurrent;
        lIsSet := true;
      end;
    end;
  finally
    freeAndNil(lEnum);
  end;
end;

function TWeather5DayForecast.getMinimumTemperature() : Double;
var lWeather : I5DayForecastWeather;
begin
  result   := 0;
  lWeather := getMinimum();
  if assigned(lWeather) then result := lWeather.getTemperature();
end;

function TWeather5DayForecast.getMaximum() : I5DayForecastWeather;
var lEnum    : TWeatherObjectList.TEnumerator;
    lWeather : I5DayForecastWeather;
    lIsSet   : Boolean;
    lCurrent,
    lTemp    : Double;
begin
  lTemp  := 0;
  result := nil;

  lEnum := list.GetEnumerator();
  try
    lIsSet := false;
    while lEnum.MoveNext() do
    begin
      lWeather := lEnum.Current as I5DayForecastWeather;
      lCurrent := lWeather.getTemperature();
      if lIsSet then
      begin
        if (lCurrent > lTemp) then
        begin
          result := lWeather;
          lTemp  := lCurrent;
        end;
      end
      else
      begin
        result := lWeather;
        lTemp  := lCurrent;
        lIsSet := true;
      end;
    end;
  finally
    freeAndNil(lEnum);
  end;
end;

function TWeather5DayForecast.getMaximumTemperature() : Double;
var lWeather : I5DayForecastWeather;
begin
  result   := 0;
  lWeather := getMaximum();
  if assigned(lWeather) then result := lWeather.getTemperature();
end;











function TWeatherStationInfo.getName() : String;
begin
  result := getJSONProperty('name').AsString();
end;

procedure TWeatherStationInfo.setName(const pValue : String);
begin
  setJSONProperty('name', TValue.From<String>(pValue));
end;

function TWeatherStationInfo.getType() : Integer;
begin
  result := getJSONProperty('type').AsInteger();
end;

procedure TWeatherStationInfo.setType(const pValue : Integer);
begin
  setJSONProperty('type', TValue.From<Integer>(pValue));
end;

function TWeatherStationInfo.getStatus() : Integer;
begin
  result := getJSONProperty('status').AsInteger();
end;

procedure TWeatherStationInfo.setStatus(const pValue : Integer);
begin
  setJSONProperty('status', TValue.From<Integer>(pValue));
end;

function TWeatherStationInfo.getId() : Integer;
begin
  result := getJSONProperty('id').AsInteger();
end;

procedure TWeatherStationInfo.setId(const pValue : Integer);
begin
  setJSONProperty('id', TValue.From<Integer>(pValue));
end;

function TWeatherStationInfo.getCoord() : IWeatherCoord;
begin
  if not assigned(fCoord) then
    fCoord := getIntfProperty('coord', TWeatherCoord) as IWeatherCoord;

  result := fCoord;
end;

procedure TWeatherStationInfo.setCoord(const pValue : IWeatherCoord);
begin
  fCoord := pValue;
end;












function TWeatherStation.getStation() : IWeatherStationInfo;
begin
  if not assigned(fStation) then
    fStation := getIntfProperty('station', TWeatherStationInfo) as IWeatherStationInfo;

  result := fStation;
end;

procedure TWeatherStation.setStation(const pValue : IWeatherStationInfo);
begin
  fStation := pValue;
end;

function TWeatherStation.getDistance() : Double;
begin
  result := getJSONProperty('distance').AsExtended();
end;

procedure TWeatherStation.setDistance(const pValue : Double);
begin
  setJSONProperty('distance', TValue.From<Double>(pValue));
end;

function TWeatherStation.getLast() : ICurrentWeather;
begin
  if not assigned(fLast) then
    fLast := getIntfProperty('last', TCurrentWeather) as ICurrentWeather;

  result := fLast;
end;

procedure TWeatherStation.setLast(const pValue : ICurrentWeather);
begin
  fLast := pValue;
end;

end.
