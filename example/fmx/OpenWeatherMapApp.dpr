program OpenWeatherMapApp;

uses
  System.StartUpCopy,
  FMX.Forms,
  Unit1 in 'Unit1.pas' {TOpenWeatherMapWin},
  OpenWeatherMapInterfaces in '..\..\OpenWeatherMapInterfaces.pas',
  OpenWeatherMapTypes in '..\..\OpenWeatherMapTypes.pas',
  OpenWeatherMapClient in '..\..\OpenWeatherMapClient.pas',
  OpenWeatherMap in '..\..\OpenWeatherMap.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TTOpenWeatherMapWin, TOpenWeatherMapWin);
  Application.Run;
end.
