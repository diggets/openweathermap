(*
 *  Copyright 2016 Eric Berger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *)

unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects, FMX.TabControl, FMX.Edit,
  FMX.ListBox, OpenWeatherMapInterfaces, OpenWeatherMap, FMXTee.Engine,
  FMXTee.Series, FMXTee.Procs, FMXTee.Chart, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.ListView.Adapters.Base, FMX.ListView,
  FMX.Layouts, FMX.EditBox, FMX.NumberBox;

const
  WEATHER_UNIT_SYMBOL_DEFAULT  = 'K';
  WEATHER_UNIT_SYMBOL_METRIC   = '�';
  WEATHER_UNIT_SYMBOL_IMPERIAL = '�F';

type
  TTOpenWeatherMapWin = class(TForm)
    Img_Weather: TImage;
    Lbl_Temperature: TLabel;
    Lbl_Location: TLabel;
    Lbl_Timestamp: TLabel;
    TbCtrl_Weather: TTabControl;
    TbItm_ByCity: TTabItem;
    TbItm_ByCoords: TTabItem;
    TbItm_ById: TTabItem;
    TbItm_ByZipcode: TTabItem;
    Edt_City: TEdit;
    EdtLbl_City: TLabel;
    CmbBx_CityUnit: TComboBox;
    CmbBxLbl_Unit: TLabel;
    CmbBx_CityLang: TComboBox;
    CmbBxLbl_Language: TLabel;
    Btn_ByCity: TButton;
    Edt_Longitude: TEdit;
    EdtLbl_Longitude: TLabel;
    CmbBx_CoordsUnit: TComboBox;
    CmbBxLbl_CoordsUnit: TLabel;
    CmbBx_CoordsLang: TComboBox;
    CmbBxLbl_CoordsLang: TLabel;
    Btn_ByCoords: TButton;
    Edt_Latitude: TEdit;
    EdtLbl_Latitude: TLabel;
    Btn_ByID: TButton;
    CmbBx_IDLang: TComboBox;
    CmbBxLbl_IDLang: TLabel;
    CmbBx_IDUnit: TComboBox;
    CmbBxLbl_IDUnit: TLabel;
    Edt_ID: TEdit;
    EdtLbl_ID: TLabel;
    Btn_ByZipcode: TButton;
    CmbBx_ZipLang: TComboBox;
    CmbBxLbl_ZipLang: TLabel;
    CmbBx_ZipUnit: TComboBox;
    CmbBxLbl_ZipUnit: TLabel;
    Edt_Zipcode: TEdit;
    EdtLbl_Zipcode: TLabel;
    Edt_Country: TEdit;
    EdtLbl_Country: TLabel;
    Rct_Weather: TRectangle;
    TbCtrl_Actions: TTabControl;
    TbItm_Current: TTabItem;
    TbItm_Forecast: TTabItem;
    Btn_5DayForecast: TButton;
    Chart1: TChart;
    Series1: TLineSeries;
    Lbl_ForecastInfo: TLabel;
    Lbl_MinTemp: TLabel;
    Lbl_MaxTemp: TLabel;
    TbItm_Stations: TTabItem;
    LstVw_Stations: TListView;
    Btn_Stations: TButton;
    Edt_ApiKey: TEdit;
    EdtLbl_ApiKey: TLabel;
    Edt_ForecastCity: TEdit;
    EdtLbl_ForecastCity: TLabel;
    Edt_StationLat: TEdit;
    EdtLbl_StationLat: TLabel;
    Edt_StationLong: TEdit;
    EdtLbl_StationLong: TLabel;
    ScrllBx_ByCity: TVertScrollBox;
    ScrllBx_ByCoords: TVertScrollBox;
    ScrllBx_ByID: TVertScrollBox;
    ScrllBx_ByZipcode: TVertScrollBox;
    TbItm_Transmit: TTabItem;
    ScrllBx_Transmit: TVertScrollBox;
    Edt_Username: TEdit;
    EdtLbl_Username: TLabel;
    Edt_Password: TEdit;
    EdtLbl_Password: TLabel;
    Edt_StName: TEdit;
    EdtLbl_StName: TLabel;
    Lbl_StationData: TLabel;
    Edt_StWindDir: TNumberBox;
    EdtLbl_StWindDir: TLabel;
    Edt_StWindSpeed: TNumberBox;
    EdtLbl_StWindSpeed: TLabel;
    Edt_StWindGust: TNumberBox;
    EdtLbl_WindGust: TLabel;
    Edt_StTemp: TNumberBox;
    EdtLbl_StTemp: TLabel;
    Edt_StHumidity: TNumberBox;
    EdtLbl_StHumidity: TLabel;
    Edt_StPressure: TNumberBox;
    EdtLbl_StPressure: TLabel;
    Edt_StRain1h: TNumberBox;
    EdtLbl_StRain1h: TLabel;
    Edt_StRain24h: TNumberBox;
    EdtLbl_StRain24h: TLabel;
    Edt_StRainToday: TNumberBox;
    EdtLbl_StRainToday: TLabel;
    Edt_StSnow: TNumberBox;
    EdtLbl_StSnow: TLabel;
    Edt_StLum: TNumberBox;
    EdtLbl_StLum: TLabel;
    Edt_StLong: TNumberBox;
    EdtLbl_StLong: TLabel;
    Edt_StLat: TNumberBox;
    EdtLbl_StLat: TLabel;
    Edt_StAlt: TNumberBox;
    EdtLbl_StAlt: TLabel;
    Edt_StRadiation: TNumberBox;
    EdtLbl_StRadiation: TLabel;
    Edt_StDewPoint: TNumberBox;
    EdtLbl_StDewPoint: TLabel;
    Edt_StUV: TNumberBox;
    EdtLbl_StUV: TLabel;
    Btn_Transmit: TButton;
    PasswordEditButton1: TPasswordEditButton;
    procedure Tmr_CurrentWeatherTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Btn_ByCityClick(Sender: TObject);
    procedure Btn_ByCoordsClick(Sender: TObject);
    procedure Btn_ByIDClick(Sender: TObject);
    procedure Btn_ByZipcodeClick(Sender: TObject);
    procedure Btn_5DayForecastClick(Sender: TObject);
    procedure Btn_StationsClick(Sender: TObject);
    procedure Btn_TransmitClick(Sender: TObject);
  private
    fOpenWeatherMap : TOpenWeatherMap;
  public
    procedure reset();
    procedure loadWeather(const pWeather : ICurrentWeather;
                          const pUnit : String = WEATHER_UNIT_SYMBOL_METRIC);
  end;

var
  TOpenWeatherMapWin: TTOpenWeatherMapWin;

implementation

{$R *.fmx}

uses
  OpenWeatherMapClient;

procedure TTOpenWeatherMapWin.Btn_5DayForecastClick(Sender: TObject);
var lForecast : IWeather5DayForecast;
    lEnum     : TWeatherObjectList.TEnumerator;
    lWeather  : I5DayForecastWeather;
begin
  Series1.Clear();

  if Edt_ForecastCity.Text.IsEmpty() then
  begin
    ShowMessage('No city defined for forecast.');
    exit;
  end;

  fOpenWeatherMap.client.api_key := Edt_ApiKey.Text;
  lForecast := fOpenWeatherMap.client.get5DayForecast(Edt_ForecastCity.Text, '', '', 'metric', 'en');
  lEnum := lForecast.list.GetEnumerator();
  try
    while lEnum.MoveNext() do
    begin
      lWeather := lEnum.Current as I5DayForecastWeather;
      Series1.Add(lWeather.getTemperature(), lWeather.dt_txt);
    end;
  finally
    freeAndNil(lEnum);
  end;

  lWeather := lForecast.getMinimum();
  if assigned(lWeather) then
  begin
    Lbl_MinTemp.Text := 'Minimum Temperature: ' + formatFloat('#0.0', lWeather.getTemperature()) + '� (' +
                        FormatDateTime('dd.mm.yyyy hh:nn:ss', lWeather.getTimestamp(), TOpenWeatherMapClient.FORMATSETTINGS) +
                        ')';
  end
  else Lbl_MinTemp.Text := 'Minimum Temperature: 0.0�';

  lWeather := lForecast.getMaximum();
  if assigned(lWeather) then
  begin
    Lbl_MaxTemp.Text := 'Maximum Temperature: ' + formatFloat('#0.0', lWeather.getTemperature()) + '� (' +
                        FormatDateTime('dd.mm.yyyy hh:nn:ss', lWeather.getTimestamp(), TOpenWeatherMapClient.FORMATSETTINGS) +
                        ')';
  end
  else Lbl_MaxTemp.Text := 'Maximum Temperature: 0.0�';
end;

procedure TTOpenWeatherMapWin.Btn_ByCityClick(Sender: TObject);
var lWeather : ICurrentWeather;
    lUnit    : String;
begin
  try
    if Edt_City.Text.IsEmpty() then
      raise EAbort.Create('missing city name');

    if not assigned(CmbBx_CityUnit.Selected) then
      raise EAbort.Create('unit not selected');

    if not assigned(CmbBx_CityLang.Selected) then
      raise EAbort.Create('language not selected');
  except
    on e:exception do
    begin
      ShowMessage(e.Message);
      exit;
    end;
  end;

  try
    fOpenWeatherMap.client.api_key := Edt_ApiKey.Text;
    lWeather := fOpenWeatherMap.client.getCurrentWeather(Edt_City.Text, '', '',
                                                         CmbBx_CityUnit.Selected.Text,
                                                         CmbBx_CityLang.Selected.Text);

    if CmbBx_CityUnit.Selected.Text.Equals('default') then lUnit := WEATHER_UNIT_SYMBOL_DEFAULT
    else if CmbBx_CityUnit.Selected.Text.Equals('imperial') then lUnit := WEATHER_UNIT_SYMBOL_IMPERIAL
    else lUnit := WEATHER_UNIT_SYMBOL_METRIC;

    loadWeather(lWeather, lUnit);
  except
    on e:exception do
    begin
      reset();
      ShowMessage(e.Message);
    end;
  end;
end;

procedure TTOpenWeatherMapWin.Btn_ByCoordsClick(Sender: TObject);
var lWeather : ICurrentWeather;
    lLong,
    lLat     : Double;
    lUnit    : String;
begin
  try
    if not TryStrToFloat(Edt_Longitude.Text, lLong, TOpenWeatherMapClient.FORMATSETTINGS) then
      raise EAbort.Create('longitude value not valid');

    if not TryStrToFloat(Edt_Latitude.Text, lLat, TOpenWeatherMapClient.FORMATSETTINGS) then
      raise EAbort.Create('latitude value not valid');

    if not assigned(CmbBx_CoordsUnit.Selected) then
      raise EAbort.Create('unit not selected');

    if not assigned(CmbBx_CoordsLang.Selected) then
      raise EAbort.Create('language not selected');
  except
    on e:exception do
    begin
      ShowMessage(e.Message);
      exit;
    end;
  end;

  try
    fOpenWeatherMap.client.api_key := Edt_ApiKey.Text;
    lWeather := fOpenWeatherMap.client.getCurrentWeather(lLong,
                                                         lLat,
                                                         CmbBx_CoordsUnit.Selected.Text,
                                                         CmbBx_CoordsLang.Selected.Text);

    if CmbBx_CityUnit.Selected.Text.Equals('default') then lUnit := WEATHER_UNIT_SYMBOL_DEFAULT
    else if CmbBx_CityUnit.Selected.Text.Equals('imperial') then lUnit := WEATHER_UNIT_SYMBOL_IMPERIAL
    else lUnit := WEATHER_UNIT_SYMBOL_METRIC;

    loadWeather(lWeather, lUnit);
  except
    on e:exception do
    begin
      reset();
      ShowMessage(e.Message);
    end;
  end;
end;

procedure TTOpenWeatherMapWin.Btn_ByIDClick(Sender: TObject);
var lWeather : ICurrentWeather;
    lID      : Integer;
    lUnit    : String;
begin
  try
    if not TryStrToInt(Edt_ID.Text, lID) then
      raise EAbort.Create('id value not valid');

    if not assigned(CmbBx_IDUnit.Selected) then
      raise EAbort.Create('unit not selected');

    if not assigned(CmbBx_IDLang.Selected) then
      raise EAbort.Create('language not selected');
  except
    on e:exception do
    begin
      ShowMessage(e.Message);
      exit;
    end;
  end;

  try
    fOpenWeatherMap.client.api_key := Edt_ApiKey.Text;
    lWeather := fOpenWeatherMap.client.getCurrentWeather(lID,
                                                         CmbBx_IDUnit.Selected.Text,
                                                         CmbBx_IDLang.Selected.Text);

    if CmbBx_CityUnit.Selected.Text.Equals('default') then lUnit := WEATHER_UNIT_SYMBOL_DEFAULT
    else if CmbBx_CityUnit.Selected.Text.Equals('imperial') then lUnit := WEATHER_UNIT_SYMBOL_IMPERIAL
    else lUnit := WEATHER_UNIT_SYMBOL_METRIC;

    loadWeather(lWeather, lUnit);
  except
    on e:exception do
    begin
      reset();
      ShowMessage(e.Message);
    end;
  end;
end;

procedure TTOpenWeatherMapWin.Btn_ByZipcodeClick(Sender: TObject);
var lWeather : ICurrentWeather;
    lUnit    : String;
begin
  try
    if Edt_ZipCode.Text.IsEmpty() then
      raise EAbort.Create('missing zipcode');

    if Edt_Country.Text.IsEmpty() then
      raise EAbort.Create('missing country name');

    if not assigned(CmbBx_ZipUnit.Selected) then
      raise EAbort.Create('unit not selected');

    if not assigned(CmbBx_ZipLang.Selected) then
      raise EAbort.Create('language not selected');
  except
    on e:exception do
    begin
      ShowMessage(e.Message);
      exit;
    end;
  end;

  try
    fOpenWeatherMap.client.api_key := Edt_ApiKey.Text;
    lWeather := fOpenWeatherMap.client.getCurrentWeather('',
                                                         Edt_Zipcode.Text,
                                                         Edt_Country.Text,
                                                         CmbBx_ZipUnit.Selected.Text,
                                                         CmbBx_ZipLang.Selected.Text);

    if CmbBx_CityUnit.Selected.Text.Equals('default') then lUnit := WEATHER_UNIT_SYMBOL_DEFAULT
    else if CmbBx_CityUnit.Selected.Text.Equals('imperial') then lUnit := WEATHER_UNIT_SYMBOL_IMPERIAL
    else lUnit := WEATHER_UNIT_SYMBOL_METRIC;

    loadWeather(lWeather, lUnit);
  except
    on e:exception do
    begin
      reset();
      ShowMessage(e.Message);
    end;
  end;
end;

procedure TTOpenWeatherMapWin.Btn_StationsClick(Sender: TObject);
var lItem    : TListViewItem;
    lList    : TWeatherObjectList;
    lEnum    : TWeatherObjectList.TEnumerator;
    lStation : IWeatherStation;
    lTemp    : String;
    lLong,
    lLat     : Double;
begin
  try
    if not TryStrToFloat(Edt_StationLong.Text, lLong, TOpenWeatherMapClient.FORMATSETTINGS) then
      raise EAbort.Create('longitude value not valid');

    if not TryStrToFloat(Edt_StationLat.Text, lLat, TOpenWeatherMapClient.FORMATSETTINGS) then
      raise EAbort.Create('latitude value not valid');
  except
    on e:exception do
    begin
      ShowMessage(e.Message);
      exit;
    end;
  end;

  LstVw_Stations.BeginUpdate();
  try
    LstVw_Stations.Items.Clear();

    // Kings Cross, London
    // United Kingdom
    // 51.528306, -0.131828
    fOpenWeatherMap.client.api_key := Edt_ApiKey.Text;
    lList := fOpenWeatherMap.client.getStations(lLat, lLong, 50, 'metric', 'en');
    try
      lEnum := lList.GetEnumerator();
      try
        while lEnum.MoveNext() do
        begin
          lStation := lEnum.Current as IWeatherStation;

          lItem := LstVw_Stations.Items.Add();
          lItem.Text := lStation.station.name;

          lTemp := formatFloat('#0.0', lStation.last.getTemperature()) + '� (' +
                   formatDateTime('dd.mm.yyyy hh:nn:ss', lStation.last.getTimestamp(), TOpenWeatherMapClient.FORMATSETTINGS)+
                   ')';
          lItem.Detail := lTemp;
        end;
      finally
        freeAndNil(lEnum);
      end;
    finally
      freeAndNil(lList);
    end;
  finally
    LstVw_Stations.EndUpdate();
  end;
end;

procedure TTOpenWeatherMapWin.Btn_TransmitClick(Sender: TObject);
var lData : TWeatherStationData;
begin
  // transmit weather station data
  try
    if Edt_Username.Text.IsEmpty() then
      raise EAbort.Create('missing username');

    if Edt_Password.Text.IsEmpty() then
      raise EAbort.Create('missing password');

    if Edt_StName.Text.IsEmpty() then
      raise EAbort.Create('missing station name');
  except
    on e:exception do
    begin
      ShowMessage(e.Message);
      exit;
    end;
  end;

  try
    fOpenWeatherMap.client.username := Edt_Username.Text;
    fOpenWeatherMap.client.password := Edt_Password.Text;

    lData.name       := Edt_StName.Text;
    lData.long       := Edt_StLong.Value;
    lData.lat        := Edt_StLat.Value;
    lData.alt        := Edt_StAlt.Value;

    lData.wind_dir   := Edt_StWindDir.Value;
    lData.wind_speed := Edt_StWindSpeed.Value;
    lData.wind_gust  := Edt_StWindGust.Value;

    lData.temp       := Edt_StTemp.Value;
    lData.humidity   := Edt_StHumidity.Value;
    lData.pressure   := Edt_StPressure.Value;

    lData.rain_1h    := round(Edt_StRain1h.Value);
    lData.rain_24h   := round(Edt_StRain24h.Value);
    lData.rain_today := round(Edt_StRainToday.Value);

    lData.snow       := round(Edt_StSnow.Value);

    lData.lum        := Edt_StLum.Value;
    lData.radiation  := Edt_StRadiation.Value;
    lData.dewpoint   := Edt_StDewPoint.Value;
    lData.uv         := round(Edt_StUV.Value);

    if not fOpenWeatherMap.client.transmit(lData) then
      raise EAbort.Create('weather station data transmission failed')
    else
      ShowMessage('Weather station data successfully transmitted!');
  except
    on e:exception do
    begin
      ShowMessage(e.Message);
      exit;
    end;
  end;
end;

procedure TTOpenWeatherMapWin.FormCreate(Sender: TObject);
begin
  Series1.Clear();
  Lbl_MinTemp.Text := 'Minimum Temperature: 0.0�';
  Lbl_MaxTemp.Text := 'Maximum Temperature: 0.0�';

  fOpenWeatherMap := TOpenWeatherMap.Create(Self);
//  fOpenWeatherMap.client.api_key := '';
end;

procedure TTOpenWeatherMapWin.FormDestroy(Sender: TObject);
begin
  freeAndNil(fOpenWeatherMap);
end;

procedure TTOpenWeatherMapWin.Tmr_CurrentWeatherTimer(Sender: TObject);
begin
  loadWeather(nil);
end;

procedure TTOpenWeatherMapWin.reset();
begin
  Img_Weather.Bitmap.LoadFromFile('..\images\default.png');

  Lbl_Temperature.Text := '0.0�';
  Lbl_Location.Text    := 'Undefined';
  Lbl_Timestamp.Text   := FormatDateTime('dd.mm.yyyy hh:nn:ss', 0, TOpenWeatherMapClient.FORMATSETTINGS);
end;

procedure TTOpenWeatherMapWin.loadWeather(const pWeather : ICurrentWeather;
                                          const pUnit : String = WEATHER_UNIT_SYMBOL_METRIC);
var lFile : String;
begin
  if assigned(pWeather) then
  begin
    lFile := '..\images\' + pWeather.getIconFile();
    if fileExists(lFile) then Img_Weather.Bitmap.LoadFromFile(lFile)
                         else Img_Weather.Bitmap.LoadFromFile('..\images\default.png');

    Lbl_Temperature.Text := FormatFloat('#0.0', pWeather.getTemperature()) + pUnit;
    Lbl_Location.Text    := pWeather.getLocationName();
    Lbl_Timestamp.Text   := FormatDateTime('dd.mm.yyyy hh:nn:ss', pWeather.getTimestamp(), TOpenWeatherMapClient.FORMATSETTINGS);
  end
  else reset();
end;

end.
