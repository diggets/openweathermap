# OpenWeatherMap

A simple REST wrapper library for the OpenWeatherMap API. The source code is provide for Delphi DX 10.1+.
The library was built in 2016 and I'm not sure if this is still working, but maybe some of you may help it.

*
*  Copyright 2016 Eric Berger
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*

*****************************************************************************
*** OpenWeatherMap API Client ***********************************************
*****************************************************************************
It's an easy to use component with a few simple methods to request weather
information by the OpenWeatherMap-API at: http://openweathermap.org (*)

You'll need an individual API-KEY!
For that you have to register for free at:
https://home.openweathermap.org/users/sign_up (*)

Example:
--------
procedure TForm1.FormCreate(Sender: TObject);
var lWeather : ICurrentWeather;
begin
  fOpenWeatherMap := TOpenWeatherMap.Create(Self);
  fOpenWeatherMap.client.api_key := '<YOUR-API-KEY-HERE>';
  lWeather := fOpenWeatherMap.client.getCurrentWeather('London', '', '', 'metric', 'en');
end;


(*) These links are being provided as a convenience and for informational purposes only; 
they do not constitute an endorsement or an approval by this blog of any of the products, 
services or opinions of the corporation or organization or individual. This blog bears no 
responsibility for the accuracy, legality or content of the external site or for that of 
subsequent links. Contact the external site for answers to questions regarding its content.