(*
 *  Copyright 2016 Eric Berger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *)

unit OpenWeatherMap;

interface

uses
  System.Classes,
  OpenWeatherMapInterfaces;
  
type
  (*
   * OpenWeatherMap Component
   *
   * This component is used to encapsulate the interface for the
   * OpenWeatherMapClient.
   * We use a wrapper component to divide the client from a gui usage.
   * So we could possible use the client interface as result type in external
   * libraries.
   *)
  TOpenWeatherMap = class(TComponent)
	protected
	  fClient : IOpenWeatherMapClient;
	  
	public
	  property client : IOpenWeatherMapClient read fClient;
	  
	  constructor Create(AOwner : TComponent); override;
	  destructor  Destroy(); override;
  end;
  
implementation

uses
  OpenWeatherMapClient;

procedure Register;
begin
  System.Classes.RegisterComponents('TOpenWeatherMap', [TOpenWeatherMap]);
end;

constructor TOpenWeatherMap.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  
  fClient := TOpenWeatherMapClient.Create() as IOpenWeatherMapClient;
end;

destructor TOpenWeatherMap.Destroy();
begin
  fClient := nil;
  
  inherited Destroy();
end;

end.