(*
 *  Copyright 2016 Eric Berger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *)
 
(*
	CHANGE-LOG:
	
	1.0.1 - 2016-05-01
			
			- username & password setable for weather station transmission
			- transmit() method implemented, to transmit your weather station data
			  to OpenWeatherMap.
			- api_url and transmit_url configurable properties
			
	
	1.0.0 - 2016-04-30
			
			Basic requests implemented and tested.
*)

unit OpenWeatherMapClient;

interface

uses
  System.Classes,
  System.SysUtils,
  System.JSON,
  IPPeerClient,
  REST.Types,
  REST.Client,
  REST.Authenticator.Basic,
  OpenWeatherMapInterfaces,
  OpenWeatherMapTypes;

type
  TOpenWeatherMapAddParamsProc = reference to procedure(const pParams : TRESTRequestParameterList);

  TOpenWeatherMapClient = class(TInterfacedObject,
                				        IOpenWeatherMapClient)
    private
    protected
	    fApiUrl		 : String;
	    fTransmitUrl   : String;
	  
      fRestClient 	 : TRestClient;
      fAPIKey     	 : String;
	    fAuthenticator : THTTPBasicAuthenticator;

	    function  getApiUrl() : String; virtual;
      procedure setApiUrl(const pValue: String); virtual;
	    function  getTransmitUrl() : String; virtual;
      procedure setTransmitUrl(const pValue: String); virtual;
	  
      function  getAPIKey() : String; virtual;
      procedure setAPIKey(const pValue: String); virtual;
      function  getUsername() : String; virtual;
      procedure setUsername(const pValue: String); virtual;
      function  getPassword() : String; virtual;
      procedure setPassword(const pValue: String); virtual;

      function query(const pResource : String;
                     const pClass : TWeatherObjectClass;
                     const pAddParamsProc : TOpenWeatherMapAddParamsProc = nil;
                     const pMethod : TRESTRequestMethod = TRESTRequestMethod.rmGET) : IWeatherObject; virtual;
      function queryList(const pResource : String;
                         const pClass : TWeatherObjectClass;
                         const pAddParamsProc : TOpenWeatherMapAddParamsProc = nil;
                         const pMethod : TRESTRequestMethod = TRESTRequestMethod.rmGET) : TWeatherObjectList;

    public
	    property api_url      : String read getApiUrl	     write setApiUrl;
	    property transmit_url : String read getTransmitUrl write setTransmitUrl;
	  
      property api_key  : String read getAPIKey   write setAPIKey;
      property username : String read getUsername write setUsername;
      property password : String read getPassword write setPassword;

      class var FORMATSETTINGS : System.SysUtils.TFormatSettings;

      constructor Create(); virtual;
      destructor  Destroy(); override;

      // *** Current weather
      function getCurrentWeather(const pCity : String;
                                 const pZipCode : String;
                                 const pCountry : String;
                                 const pUnits : String = 'metric';
                                 const pLang : String = 'de') : ICurrentWeather; overload; virtual;
      function getCurrentWeather(const pLongitude : Double;
                                 const pLatitude : Double;
                                 const pUnits : String = 'metric';
                                 const pLang : String = 'de') : ICurrentWeather; overload; virtual;
      function getCurrentWeather(const pId : Integer;
                                 const pUnits : String = 'metric';
                                 const pLang : String = 'de') : ICurrentWeather; overload; virtual;

      // *** 5-day forecast every 3 hours
      function get5DayForecast(const pCity : String;
                               const pZipCode : String;
                               const pCountry : String;
                               const pUnits : String = 'metric';
                               const pLang : String = 'de') : IWeather5DayForecast; overload; virtual;
      function get5DayForecast(const pLongitude : Double;
                               const pLatitude : Double;
                               const pUnits : String = 'metric';
                               const pLang : String = 'de') : IWeather5DayForecast; overload; virtual;
      function get5DayForecast(const pId : Integer;
                               const pUnits : String = 'metric';
                               const pLang : String = 'de') : IWeather5DayForecast; overload; virtual;

      // *** station request
      function getStation(const pID : Integer;
                          const pUnits : String = 'metric';
                          const pLang : String = 'de') : IWeatherStation; overload;
      function getStations(const pBBox : TWeatherBBox;
                           const pCluster : String = WEATHER_CLUSTER_NO;
                           const pCnt : Integer = 100;
                           const pUnits : String = 'metric';
                           const pLang : String = 'de') : TWeatherObjectList; overload;
      function getStations(const pLatitude : Double;
                           const pLongitude : Double;
                           const pCnt : Integer = 25;
                           const pUnits : String = 'metric';
                           const pLang : String = 'de') : TWeatherObjectList; overload;

      function transmit(const pData : TWeatherStationData) : Boolean;
  end;

implementation

uses
  System.Rtti,
  System.TypInfo;

constructor TOpenWeatherMapClient.Create();
begin
  inherited Create();
  
  fApiUrl 	   := 'api.openweathermap.org/data/2.5/';
  fTransmitUrl := 'http://openweathermap.org/';
  
  fRestClient    := TRestClient.Create(nil);  
  fAuthenticator := THTTPBasicAuthenticator.Create('', '');
end;

destructor TOpenWeatherMapClient.Destroy();
begin
  // we need to destroy the authenticator by ourself
  fRestClient.Authenticator := nil;
  freeAndNil(fAuthenticator);

  freeAndNil(fRestClient);
  
  inherited Destroy();
end;

function TOpenWeatherMapClient.getApiUrl() : String;
begin
  result := fApiUrl;
end;

procedure TOpenWeatherMapClient.setApiUrl(const pValue: String);
begin
  fApiUrl := pValue;
end;

function TOpenWeatherMapClient.getTransmitUrl() : String;
begin
  result := fTransmitUrl;
end;

procedure TOpenWeatherMapClient.setTransmitUrl(const pValue: String);
begin
  fTransmitUrl := pValue;
end;

function TOpenWeatherMapClient.getAPIKey() : String;
begin
  result := fAPIKey;
end;

procedure TOpenWeatherMapClient.setAPIKey(const pValue: String);
begin
  fAPIKey := pValue;
end;

function TOpenWeatherMapClient.getUsername() : String;
begin
  result := fAuthenticator.username;
end;

procedure TOpenWeatherMapClient.setUsername(const pValue: String);
begin
  fAuthenticator.username := pValue;
end;

function TOpenWeatherMapClient.getPassword() : String;
begin
  result := fAuthenticator.password;
end;

procedure TOpenWeatherMapClient.setPassword(const pValue: String);
begin
  fAuthenticator.password := pValue;
end;

function TOpenWeatherMapClient.query(const pResource : String;
                                     const pClass : TWeatherObjectClass;
                                     const pAddParamsProc : TOpenWeatherMapAddParamsProc = nil;
                                     const pMethod : TRESTRequestMethod = TRESTRequestMethod.rmGET) : IWeatherObject;
var lReq  : TRESTRequest;
    lRes  : TCustomRESTResponse;
    lIntf : IWeatherObject;
    lErr  : IWeatherError;
begin
  result := nil;

  // api.openweathermap.org/data/2.5/weather?q=London
  // api.openweathermap.org/data/2.5/weather?q=London,uk
  fRestClient.Authenticator := nil;
  fRestClient.BaseURL := fApiUrl;
  lReq := TRESTRequest.Create(nil);
  try
    lReq.Client   := fRestClient;
    lReq.Resource := pResource;
    lReq.Method   := pMethod;

    if assigned(pAddParamsProc) then
      pAddParamsProc(lReq.Params);

    // by default add "APPID" parameter
    lReq.Params.AddItem('APPID', fAPIKey, TRESTRequestParameterKind.pkGETorPOST);

    lReq.Execute();
    lRes := lReq.Response;

    // analyze response
    if assigned(lRes) then
    begin
      lIntf := pClass.fromJson(lRes.Content) as IWeatherObject;
      if Supports(lIntf, IWeatherError) then
      begin
        lErr := lIntf as IWeatherError;
        raise EAbort.CreateFmt('%s [%s]', [lErr.message, lErr.cod])
      end
      else if not Supports(lIntf, IWeatherObject) then
        raise EAbort.Create('result do not support interface')
      else result := lIntf;
    end;
  finally
    freeAndNil(lReq);
  end;
end;

function TOpenWeatherMapClient.queryList(const pResource : String;
                                         const pClass : TWeatherObjectClass;
                                         const pAddParamsProc : TOpenWeatherMapAddParamsProc = nil;
                                         const pMethod : TRESTRequestMethod = TRESTRequestMethod.rmGET) : TWeatherObjectList;
var lReq  : TRESTRequest;
    lRes  : TCustomRESTResponse;
    lIntf : IWeatherObject;
    lErr  : IWeatherError;
    lVal  : TJSONValue;
    lArr  : TJSONArray;
    lEnum : TJSONArrayEnumerator;
begin
  result := TWeatherObjectList.Create();

  fRestClient.Authenticator := nil;
  fRestClient.BaseURL := fApiUrl;
  lReq := TRESTRequest.Create(nil);
  try
    lReq.Client   := fRestClient;
    lReq.Resource := pResource;
    lReq.Method   := pMethod;

    if assigned(pAddParamsProc) then
      pAddParamsProc(lReq.Params);

    // by default add "APPID" parameter
    lReq.Params.AddItem('APPID', fAPIKey, TRESTRequestParameterKind.pkGETorPOST);

    lReq.Execute();
    lRes := lReq.Response;

    // analyze response
    if assigned(lRes) then
    begin
      lVal := TJSONObject.ParseJSONValue(lRes.Content);
      if (lVal is TJSONArray) then
      begin
        lArr  := TJSONArray(lVal);
        lEnum := lArr.GetEnumerator();
        try
          while lEnum.MoveNext() do
          begin
            lVal := lEnum.Current;

            if (lVal is TJSONObject) then
            begin
              lIntf := pClass.fromJSON(TJSONObject(lVal));
              if Supports(lIntf, IWeatherError) then
              begin
                lErr := lIntf as IWeatherError;
                raise EAbort.CreateFmt('%s [%s]', [lErr.message, lErr.cod])
              end
              else result.Add(lIntf);
            end;
          end;
        finally
          freeAndNil(lEnum);
        end;
      end
      else if (lVal is TJSONObject) then
      begin
        raise Exception.Create('unexpected result - array expected');
      end
      else raise Exception.Create('unexpected result');
    end;
  finally
    freeAndNil(lReq);
  end;
end;

function TOpenWeatherMapClient.getCurrentWeather(const pCity : String;
                                                 const pZipCode : String;
												                         const pCountry : String;
                                                 const pUnits : String = 'metric';
                                                 const pLang : String = 'de') : ICurrentWeather;
begin
  result := query('weather',
                  TCurrentWeather,
                  procedure(const pParams : TRESTRequestParameterList)
                  begin
                    if (not pZipCode.IsEmpty()) and (not pCountry.IsEmpty()) then
                      pParams.AddItem('zip', format('%s,%s', [pZipCode, pCountry]), TRESTRequestParameterKind.pkGETorPOST)
                    else if (not pCity.IsEmpty()) and (not pCountry.IsEmpty()) then
                      pParams.AddItem('q', format('%s,%s', [pCity, pCountry]), TRESTRequestParameterKind.pkGETorPOST)
                    else
                      pParams.AddItem('q', pCity, TRESTRequestParameterKind.pkGETorPOST);

                    pParams.AddItem('units', pUnits, TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('lang', pLang, TRESTRequestParameterKind.pkGETorPOST);
                  end,
                  TRESTRequestMethod.rmGET) as ICurrentWeather;
end;

function TOpenWeatherMapClient.getCurrentWeather(const pLongitude : Double;
                                                 const pLatitude : Double;
                                                 const pUnits : String = 'metric';
                                                 const pLang : String = 'de') : ICurrentWeather;
begin
  result := query('weather',
                  TCurrentWeather,
                  procedure(const pParams : TRESTRequestParameterList)
                  begin
                    pParams.AddItem(
                                  'lat',
                                  formatFloat('0.00000000', pLatitude, TOpenWeatherMapClient.FORMATSETTINGS),
                                  TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem(
                                  'lon',
                                  formatFloat('0.00000000', pLongitude, TOpenWeatherMapClient.FORMATSETTINGS),
                                  TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('units', pUnits, TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('lang', pLang, TRESTRequestParameterKind.pkGETorPOST);
                  end,
                  TRESTRequestMethod.rmGET) as ICurrentWeather;
end;

function TOpenWeatherMapClient.getCurrentWeather(const pId : Integer;
                                                 const pUnits : String = 'metric';
                                                 const pLang : String = 'de') : ICurrentWeather;
begin
  result := query('weather',
                  TCurrentWeather,
                  procedure(const pParams : TRESTRequestParameterList)
                  begin
                    pParams.AddItem('id', IntToStr(pId), TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('units', pUnits, TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('lang', pLang, TRESTRequestParameterKind.pkGETorPOST);
                  end,
                  TRESTRequestMethod.rmGET) as ICurrentWeather;
end;

function TOpenWeatherMapClient.get5DayForecast(const pCity : String;
                                               const pZipCode : String;
                                               const pCountry : String;
                                               const pUnits : String = 'metric';
                                               const pLang : String = 'de') : IWeather5DayForecast;
begin
  result := query('forecast',
                  TWeather5DayForecast,
                  procedure(const pParams : TRESTRequestParameterList)
                  begin
                    if (not pZipCode.IsEmpty()) and (not pCountry.IsEmpty()) then
                      pParams.AddItem('zip', format('%s,%s', [pZipCode, pCountry]), TRESTRequestParameterKind.pkGETorPOST)
                    else if (not pCity.IsEmpty()) and (not pCountry.IsEmpty()) then
                      pParams.AddItem('q', format('%s,%s', [pCity, pCountry]), TRESTRequestParameterKind.pkGETorPOST)
                    else
                      pParams.AddItem('q', pCity, TRESTRequestParameterKind.pkGETorPOST);

                    pParams.AddItem('units', pUnits, TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('lang', pLang, TRESTRequestParameterKind.pkGETorPOST);
                  end,
                  TRESTRequestMethod.rmGET) as IWeather5DayForecast;
end;

function TOpenWeatherMapClient.get5DayForecast(const pLongitude : Double;
                                               const pLatitude : Double;
                                               const pUnits : String = 'metric';
                                               const pLang : String = 'de') : IWeather5DayForecast;
begin
  result := query('forecast',
                  TWeather5DayForecast,
                  procedure(const pParams : TRESTRequestParameterList)
                  begin
                    pParams.AddItem(
                                  'lat',
                                  formatFloat('0.00000000', pLatitude, TOpenWeatherMapClient.FORMATSETTINGS),
                                  TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem(
                                  'lon',
                                  formatFloat('0.00000000', pLongitude, TOpenWeatherMapClient.FORMATSETTINGS),
                                  TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('units', pUnits, TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('lang', pLang, TRESTRequestParameterKind.pkGETorPOST);
                  end,
                  TRESTRequestMethod.rmGET) as IWeather5DayForecast;
end;

function TOpenWeatherMapClient.get5DayForecast(const pId : Integer;
                                               const pUnits : String = 'metric';
                                               const pLang : String = 'de') : IWeather5DayForecast;
begin
  result := query('forecast',
                  TWeather5DayForecast,
                  procedure(const pParams : TRESTRequestParameterList)
                  begin
                    pParams.AddItem('id', IntToStr(pId), TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('units', pUnits, TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('lang', pLang, TRESTRequestParameterKind.pkGETorPOST);
                  end,
                  TRESTRequestMethod.rmGET) as IWeather5DayForecast;
end;

function TOpenWeatherMapClient.getStation(const pID : Integer;
                                          const pUnits : String = 'metric';
                                          const pLang : String = 'de') : IWeatherStation;
begin
  // *** Call current weather from one station
  // http://api.openweathermap.org/data/2.5/station?id=29584
  result := query('station',
                  TWeatherStation,
                  procedure(const pParams : TRESTRequestParameterList)
                  begin
                    pParams.AddItem('id', IntToStr(pID), TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('units', pUnits, TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('lang', pLang, TRESTRequestParameterKind.pkGETorPOST);
                  end,
                  TRESTRequestMethod.rmGET) as IWeatherStation;
end;

function TOpenWeatherMapClient.getStations(const pBBox : TWeatherBBox;
                                           const pCluster : String = WEATHER_CLUSTER_NO;
                                           const pCnt : Integer = 100;
                                           const pUnits : String = 'metric';
                                           const pLang : String = 'de') : TWeatherObjectList;
var lBBoxStr : String;
begin
  // *** Weather stations within a rectangular zone
  // http://api.openweathermap.org/data/2.5/box/station?cluster=no&cnt=200&format=json&bbox=8.87,49.07,65.21,61.26,6&APPID=
  lBBoxStr := System.SysUtils.Format(
                '%g,%g,%g,%g,%g',
                [
                pBBox.lon_top_left,
                pBBox.lat_top_left,
                pBBox.lon_bottom_right,
                pBBox.lat_bottom_right,
                pBBox.map_zoom
                ]
                );

  result := queryList('station',
                  TWeatherStation,
                  procedure(const pParams : TRESTRequestParameterList)
                  begin
                    pParams.AddItem('cluster', pCluster, TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('cnt', IntToStr(pCnt), TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('format', 'json', TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('bbox', lBBoxStr, TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('units', pUnits, TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('lang', pLang, TRESTRequestParameterKind.pkGETorPOST);
                  end,
                  TRESTRequestMethod.rmGET);
end;

function TOpenWeatherMapClient.getStations(const pLatitude : Double;
                                           const pLongitude : Double;
                                           const pCnt : Integer = 25;
                                           const pUnits : String = 'metric';
                                           const pLang : String = 'de') : TWeatherObjectList;
begin
  // *** Weather stations around a geo point
  // http://api.openweathermap.org/data/2.5/station/find?lat=55&lon=37&cnt=30
  result := queryList('station/find',
                  TWeatherStation,
                  procedure(const pParams : TRESTRequestParameterList)
                  begin
                    pParams.AddItem(
                      'lat',
                      formatFloat('0.00000000', pLatitude, TOpenWeatherMapClient.FORMATSETTINGS),
                      TRESTRequestParameterKind.pkGETorPOST
                    );
                    pParams.AddItem(
                      'lon',
                      formatFloat('0.00000000', pLongitude, TOpenWeatherMapClient.FORMATSETTINGS),
                      TRESTRequestParameterKind.pkGETorPOST
                    );
                    pParams.AddItem('cnt', IntToStr(pCnt), TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('units', pUnits, TRESTRequestParameterKind.pkGETorPOST);
                    pParams.AddItem('lang', pLang, TRESTRequestParameterKind.pkGETorPOST);
                  end,
                  TRESTRequestMethod.rmGET);
end;

function TOpenWeatherMapClient.transmit(const pData : TWeatherStationData) : Boolean;
var lReq : TRESTRequest;
    lRes : TCustomRESTResponse;
begin
  result := false;
  fRestClient.BaseURL := fTransmitUrl;
  fRestClient.Authenticator := fAuthenticator;
  lReq := TRESTRequest.Create(nil);
  try
    lReq.Client   := fRestClient;
    lReq.Resource := 'data/post';
    lReq.Method   := TRESTRequestMethod.rmPOST;
	
    // name	      : String;  // Weather station name
    lReq.Params.AddItem('name', pData.name, TRESTRequestParameterKind.pkGETorPOST);
    // wind_dir   : Double;  // Degrees Wind direction
    lReq.Params.AddItem('wind_dir', formatFloat('0.00000000', pData.wind_dir, TOpenWeatherMapClient.FORMATSETTINGS), TRESTRequestParameterKind.pkGETorPOST);
    // wind_speed : Double;  // m/s	Wind speed
    lReq.Params.AddItem('wind_speed', formatFloat('0.00000000', pData.wind_speed, TOpenWeatherMapClient.FORMATSETTINGS), TRESTRequestParameterKind.pkGETorPOST);
    // wind_gust  : Double;  // m/s	Wind gust speed
    lReq.Params.AddItem('wind_gust', formatFloat('0.00000000', pData.wind_gust, TOpenWeatherMapClient.FORMATSETTINGS), TRESTRequestParameterKind.pkGETorPOST);
    // temp       : Double;  // °C	Temperature
    lReq.Params.AddItem('temp', formatFloat('0.00000000', pData.temp, TOpenWeatherMapClient.FORMATSETTINGS), TRESTRequestParameterKind.pkGETorPOST);
    // humidity   : Double;  // RH %	Relative humidity
    lReq.Params.AddItem('humidity', formatFloat('0.00000000', pData.humidity, TOpenWeatherMapClient.FORMATSETTINGS), TRESTRequestParameterKind.pkGETorPOST);
    // pressure   : Double;  // Atmospheric pressure
    lReq.Params.AddItem('pressure', formatFloat('0.00000000', pData.pressure, TOpenWeatherMapClient.FORMATSETTINGS), TRESTRequestParameterKind.pkGETorPOST);
    // rain_1h	   : Integer; // mm	Rain in the last hour
    lReq.Params.AddItem('rain_1h', IntToStr(pData.rain_1h), TRESTRequestParameterKind.pkGETorPOST);
    // rain_24h   : Integer; // mm	Rain in the last 24 hours
    lReq.Params.AddItem('rain_24h', IntToStr(pData.rain_24h), TRESTRequestParameterKind.pkGETorPOST);
    // rain_today : Integer; // mm	Rain since midnight
    lReq.Params.AddItem('rain_today', IntToStr(pData.rain_today), TRESTRequestParameterKind.pkGETorPOST);
    // snow       : Integer; // mm	Snow in the last 24 hours
    lReq.Params.AddItem('snow', IntToStr(pData.snow), TRESTRequestParameterKind.pkGETorPOST);
    // lum	      : Double;  // W/m² Brightness
    lReq.Params.AddItem('lum', formatFloat('0.00000000', pData.lum, TOpenWeatherMapClient.FORMATSETTINGS), TRESTRequestParameterKind.pkGETorPOST);
    // lat	      : Double;  // Decimal degrees Latitude
    lReq.Params.AddItem('lat', formatFloat('0.00000000', pData.lat, TOpenWeatherMapClient.FORMATSETTINGS), TRESTRequestParameterKind.pkGETorPOST);
    // long       : Double;  // Decimal degrees Longitude
    lReq.Params.AddItem('long', formatFloat('0.00000000', pData.long, TOpenWeatherMapClient.FORMATSETTINGS), TRESTRequestParameterKind.pkGETorPOST);
    // alt        : Double;  // m Altitude
    lReq.Params.AddItem('alt', formatFloat('0.00000000', pData.alt, TOpenWeatherMapClient.FORMATSETTINGS), TRESTRequestParameterKind.pkGETorPOST);
    // radiation  : Double;  // Radiation
    lReq.Params.AddItem('radiation', formatFloat('0.00000000', pData.radiation, TOpenWeatherMapClient.FORMATSETTINGS), TRESTRequestParameterKind.pkGETorPOST);
    // dewpoint   : Double;  // °C Dew point
    lReq.Params.AddItem('dewpoint', formatFloat('0.00000000', pData.dewpoint, TOpenWeatherMapClient.FORMATSETTINGS), TRESTRequestParameterKind.pkGETorPOST);
    // uv	      : Integer; // UV index
    lReq.Params.AddItem('uv', IntToStr(pData.uv), TRESTRequestParameterKind.pkGETorPOST);

    lReq.Execute();
    lRes := lReq.Response;

    // analyze response
    if assigned(lRes) then
    begin
      result := true;
      raise EAbort.Create('RESPONSE: ' + lRes.Content);
	  end;
  finally
    freeAndNil(lReq);
  end;
end;

initialization
  TOpenWeatherMapClient.FORMATSETTINGS := System.SysUtils.TFormatSettings.Create('en-US');
  TOpenWeatherMapClient.FORMATSETTINGS.DateSeparator   := '-';
  TOpenWeatherMapClient.FORMATSETTINGS.ShortDateFormat := 'yyyy-mm-dd';
  TOpenWeatherMapClient.FORMATSETTINGS.ShortTimeFormat := 'hh:nn:ss';
  TOpenWeatherMapClient.FORMATSETTINGS.ThousandSeparator := ',';
  TOpenWeatherMapClient.FORMATSETTINGS.DecimalSeparator  := '.';

end.