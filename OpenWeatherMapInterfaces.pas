(*
 *  Copyright 2016 Eric Berger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *)

unit OpenWeatherMapInterfaces;

interface

uses
  System.Rtti,
  System.Generics.Collections;

const
  // weather units > Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit
  WEATHER_UNIT_DEFAULT  = 'kelvin';
  WEATHER_UNIT_METRIC   = 'metric';
  WEATHER_UNIT_IMPERIAL = 'imperial';

  WEATHER_CLUSTER_YES   = 'yes';
  WEATHER_CLUSTER_NO    = 'no';

type
  IWeatherObject = interface(IInterface)
  ['{4673FE5A-6656-4F7B-A313-3F27CF59D351}']
    function  ToString() : String;
	function  getJSONProperty(const pName : String) : TValue;
    procedure setJSONProperty(const pName : String;
                              const pValue : TValue);
  end;

  TWeatherObjectList = TList<IWeatherObject>;

  TWeatherBBox = record
    lon_top_left     : Double;
    lat_top_left     : Double;
    lon_bottom_right : Double;
    lat_bottom_right : Double;
    map_zoom         : Double;
  end;

  IWeatherError = interface(IWeatherObject)
  ['{3ACF28A4-5A83-459B-A512-36472AB3C25D}']
    function  getCod() : String;
    procedure setCod(const pValue : String);
    property  cod : String read getCod write setCod;

    function  getMessage() : String;
    procedure setMessage(const pValue : String);
    property  message : String read getMessage write setMessage;
  end;

  IWeatherCoord = interface(IWeatherObject)
  ['{99A04A23-487F-42D3-B156-D1110C783156}']
    function  getLon() : Double;
    procedure setLon(const pValue : Double);
    property  lon : Double read getLon write setLon;

    function  getLat() : Double;
    procedure setLat(const pValue : Double);
    property  lat : Double read getLat write setLat;
  end;

  IWeatherInfo = interface(IWeatherObject)
  ['{4FC96A55-FA6B-4C6F-B32C-1CC3E2A02E43}']
    function  getId() : Integer;
    procedure setId(const pValue : Integer);
    property  id : Integer read getId write setId;

    function  getMain() : String;
    procedure setMain(const pValue : String);
    property  main : String read getMain write setMain;

    function  getDescription() : String;
    procedure setDescription(const pValue : String);
    property  description : String read getDescription write setDescription;

    function  getIcon() : String;
    procedure setIcon(const pValue : String);
    property  icon : String read getIcon write setIcon;
  end;

  IWeatherMain = interface(IWeatherObject)
  ['{E0404CCC-4127-45B6-A019-0DC8909CFFCF}']
    function  getTemp() : Double;
    procedure setTemp(const pValue : Double);
    property  temp : Double read getTemp write setTemp;

    function  getPressure() : Double;
    procedure setPressure(const pValue : Double);
    property  pressure : Double read getPressure write setPressure;

    function  getHumidity() : Double;
    procedure setHumidity(const pValue : Double);
    property  humidity : Double read getHumidity write setHumidity;

    function  getTempMin() : Double;
    procedure setTempMin(const pValue : Double);
    property  temp_min : Double read getTempMin write setTempMin;

    function  getTempMax() : Double;
    procedure setTempMax(const pValue : Double);
    property  temp_max : Double read getTempMax write setTempMax;

    function  getSeaLevel() : Double;
    procedure setSeaLevel(const pValue : Double);
    property  sea_level : Double read getSeaLevel write setSeaLevel;

    function  getGrndLevel() : Double;
    procedure setGrndLevel(const pValue : Double);
    property  grnd_level : Double read getGrndLevel write setGrndLevel;

    function  getTempKF() : Double;
    procedure setTempKF(const pValue : Double);
    property  temp_kf : Double read getTempKF write setTempKF;
  end;

  IWeatherWind = interface(IWeatherObject)
  ['{7CD86BC3-037B-47B3-9FB9-43A0EFDA4798}']
    function  getSpeed() : Double;
    procedure setSpeed(const pValue : Double);
    property  speed : Double read getSpeed write setSpeed;

    function  getDeg() : Double;
    procedure setDeg(const pValue : Double);
    property  deg : Double read getDeg write setDeg;
  end;

  IWeatherClouds = interface(IWeatherObject)
  ['{B1F27880-293C-4308-B6D1-362874F964C0}']
    function  getAll() : Integer;
    procedure setAll(const pValue : Integer);
    property  all : Integer read getAll write setAll;
  end;

  IWeatherRain = interface(IWeatherObject)
  ['{EFC28A1A-B22A-4794-8818-E1EB862C8857}']
    function  getThreeHours() : Double;
    procedure setThreeHours(const pValue : Double);
    property  three_hours : Double read getThreeHours write setThreeHours;
  end;

  IWeatherSnow = interface(IWeatherObject)
  ['{4509F519-283A-4814-A2FC-39922FCBC19C}']
  end;

  IWeatherSys = interface(IWeatherObject)
  ['{1AB3B31B-A317-4D8D-9B75-34E039549D04}']
    function  getType() : Integer;
    procedure setType(const pValue : Integer);
    property  _type : Integer read getType write setType;

    function  getId() : Integer;
    procedure setId(const pValue : Integer);
    property  id : Integer read getId write setId;

    function  getMessage() : Double;
    procedure setMessage(const pValue : Double);
    property  message : Double read getMessage write setMessage;

    function  getCountry() : String;
    procedure setCountry(const pValue : String);
    property  country : String read getCountry write setCountry;

    function  getSunrise() : Cardinal;
    procedure setSunrise(const pValue : Cardinal);
    property  sunrise : Cardinal read getSunrise write setSunrise;

    function  getSunset() : Cardinal;
    procedure setSunset(const pValue : Cardinal);
    property  sunset : Cardinal read getSunset write setSunset;

    // optional
    function  getPod() : String;
    procedure setPod(const pValue : String);
    property  pod : String read getPod write setPod;

    // optional
    function  getPopulation() : Integer;
    procedure setPopulation(const pValue : Integer);
    property  population : Integer read getPopulation write setPopulation;
  end;

  IWeatherCity = interface(IWeatherObject)
  ['{0940D425-03D5-4C7A-9468-BFC2DA7A493A}']
    function  getId() : Integer;
    procedure setId(const pValue : Integer);
    property  id : Integer read getId write setId;

    function  getName() : String;
    procedure setName(const pValue : String);
    property  name : String read getName write setName;

    function  getCoord() : IWeatherCoord;
    procedure setCoord(const pValue : IWeatherCoord);
    property  coord : IWeatherCoord read getCoord write setCoord;

    function  getCountry() : String;
    procedure setCountry(const pValue : String);
    property  country : String read getCountry write setCountry;

    function  getPopulation() : Integer;
    procedure setPopulation(const pValue : Integer);
    property  population : Integer read getPopulation write setPopulation;

    function  getSys() : IWeatherSys;
    procedure setSys(const pValue : IWeatherSys);
    property  sys : IWeatherSys read getSys write setSys;
  end;

  ICustomWeather = interface(IWeatherObject)
  ['{06BB6791-0228-4332-B740-103F12F129D6}']
    function  getDt() : Cardinal;
    procedure setDt(const pValue : Cardinal);
    property  dt : Cardinal read getDt write setDt;

    function  getMain() : IWeatherMain;
    procedure setMain(const pValue : IWeatherMain);
    property  main : IWeatherMain read getMain write setMain;

    function  getWeather() : TWeatherObjectList;
    procedure setWeather(const pValue : TWeatherObjectList);
    property  weather : TWeatherObjectList read getWeather write setWeather;

    function  getClouds() : IWeatherClouds;
    procedure setClouds(const pValue : IWeatherClouds);
    property  clouds : IWeatherClouds read getClouds write setClouds;

    function  getWind() : IWeatherWind;
    procedure setWind(const pValue : IWeatherWind);
    property  wind : IWeatherWind read getWind write setWind;

    function  getSys() : IWeatherSys;
    procedure setSys(const pValue : IWeatherSys);
    property  sys : IWeatherSys read getSys write setSys;

    function getTemperature() : Double;
    function getTimestamp() : TDateTime;
    function getIconFile() : String;
  end;

  ICurrentWeather = interface(ICustomWeather)
  ['{2F0A2BCB-1CAC-4E10-AB69-5A5C2F1B71EE}']
    function  getCoord() : IWeatherCoord;
    procedure setCoord(const pValue : IWeatherCoord);
    property  coord : IWeatherCoord read getCoord write setCoord;

    function  getBase() : String;
    procedure setBase(const pValue : String);
    property  base : String read getBase write setBase;

    function  getVisibility() : Integer;
    procedure setVisibility(const pValue : Integer);
    property  visibility : Integer read getVisibility write setVisibility;

    function  getId() : Integer;
    procedure setId(const pValue : Integer);
    property  id : Integer read getId write setId;

    function  getName() : String;
    procedure setName(const pValue : String);
    property  name : String read getName write setName;

    function  getCod() : Integer;
    procedure setCod(const pValue : Integer);
    property  cod : Integer read getCod write setCod;

    function getLocationName() : String;
  end;

  I5DayForecastWeather = interface(ICustomWeather)
  ['{15D249E0-E0F7-4E98-A490-A6E119B4A0E0}']
    function  getRain() : IWeatherRain;
    procedure setRain(const pValue : IWeatherRain);
    property  rain : IWeatherRain read getRain write setRain;

    function  getSnow() : IWeatherSnow;
    procedure setSnow(const pValue : IWeatherSnow);
    property  snow : IWeatherSnow read getSnow write setSnow;

    function  getDtTxt() : String;
    procedure setDtTxt(const pValue : String);
    property  dt_txt : String read getDtTxt write setDtTxt;
  end;

  IWeather5DayForecast = interface(IWeatherObject)
  ['{8AB7C68C-5683-4C3C-83F5-8760FA9F564C}']
    function  getCity() : IWeatherCity;
    procedure setCity(const pValue : IWeatherCity);
    property  city : IWeatherCity read getCity write setCity;

    function  getMessage() : Double;
    procedure setMessage(const pValue : Double);
    property  message : Double read getMessage write setMessage;

    function  getCod() : String;
    procedure setCod(const pValue : String);
    property  cod : String read getCod write setCod;

    function  getCnt() : Integer;
    procedure setCnt(const pValue : Integer);
    property  cnt : Integer read getCnt write setCnt;

    function  getList() : TWeatherObjectList;
    procedure setList(const pValue : TWeatherObjectList);
    property  list : TWeatherObjectList read getList write setList;

    function getMinimum() : I5DayForecastWeather;
    function getMinimumTemperature() : Double;
    function getMaximum() : I5DayForecastWeather;
    function getMaximumTemperature() : Double;
  end;

  IWeatherStationInfo = interface(IWeatherObject)
  ['{A8508D7B-B845-44AF-A116-DB0959C9B2D0}']
    function  getName() : String;
    procedure setName(const pValue : String);
    property  name : String read getName write setName;

    function  getType() : Integer;
    procedure setType(const pValue : Integer);
    property  _type : Integer read getType write setType;

    function  getStatus() : Integer;
    procedure setStatus(const pValue : Integer);
    property  status : Integer read getStatus write setStatus;

    function  getId() : Integer;
    procedure setId(const pValue : Integer);
    property  id : Integer read getId write setId;

    function  getCoord() : IWeatherCoord;
    procedure setCoord(const pValue : IWeatherCoord);
    property  coord : IWeatherCoord read getCoord write setCoord;
  end;

  IWeatherStation = interface(IWeatherObject)
  ['{9E3BEB98-3A54-451B-BBFB-0D18FD7DEF16}']
    function  getStation() : IWeatherStationInfo;
    procedure setStation(const pValue : IWeatherStationInfo);
    property  station : IWeatherStationInfo read getStation write setStation;

    function  getDistance() : Double;
    procedure setDistance(const pValue : Double);
    property  distance : Double read getDistance write setDistance;

    function  getLast() : ICurrentWeather;
    procedure setLast(const pValue : ICurrentWeather);
    property  last : ICurrentWeather read getLast write setLast;
  end;

  TWeatherStationData = record
    wind_dir   : Double;  // Degrees Wind direction
    wind_speed : Double;  // m/s	Wind speed
    wind_gust  : Double;  // m/s	Wind gust speed
    temp       : Double;  // �C	Temperature
    humidity   : Double;  // RH %	Relative humidity
    pressure   : Double;  // Atmospheric pressure
    rain_1h	   : Integer; // mm	Rain in the last hour
    rain_24h   : Integer; // mm	Rain in the last 24 hours
    rain_today : Integer; // mm	Rain since midnight
    snow       : Integer; // mm	Snow in the last 24 hours
    lum	       : Double;  // W/m� Brightness
    lat	       : Double;  // Decimal degrees Latitude
    long       : Double;  // Decimal degrees Longitude
    alt        : Double;  // m Altitude
    radiation  : Double;  // Radiation
    dewpoint   : Double;  // �C Dew point
    uv	       : Integer; // UV index
    name	   : String;  // Weather station name
  end;
  
  IOpenWeatherMapClient = interface(IInterface)
  ['{43F8436B-AF5D-4BBA-AE86-9D581B6C603A}']
    function  getApiUrl() : String;
    procedure setApiUrl(const pValue: String);
	  property  api_url : String read getApiUrl write setApiUrl;
	  function  getTransmitUrl() : String;
    procedure setTransmitUrl(const pValue: String);
	  property  transmit_url : String read getTransmitUrl write setTransmitUrl;
	  
    function  getAPIKey() : String;
    procedure setAPIKey(const pValue: String);
    property  api_key : String read getAPIKey write setAPIKey;

    function  getUsername() : String;
    procedure setUsername(const pValue: String);
    property  username : String read getUsername write setUsername;

    function  getPassword() : String;
    procedure setPassword(const pValue: String);
    property  password : String read getPassword write setPassword;

    function getCurrentWeather(const pCity : String;
                               const pZipCode : String;
                               const pCountry : String;
                               const pUnits : String = 'metric';
                               const pLang : String = 'de') : ICurrentWeather; overload;
    function getCurrentWeather(const pLongitude : Double;
                               const pLatitude : Double;
                               const pUnits : String = 'metric';
                               const pLang : String = 'de') : ICurrentWeather; overload;
    function getCurrentWeather(const pId : Integer;
                               const pUnits : String = 'metric';
                               const pLang : String = 'de') : ICurrentWeather; overload;

    function get5DayForecast(const pCity : String;
                             const pZipCode : String;
                             const pCountry : String;
                             const pUnits : String = 'metric';
                             const pLang : String = 'de') : IWeather5DayForecast; overload;
    function get5DayForecast(const pLongitude : Double;
                             const pLatitude : Double;
                             const pUnits : String = 'metric';
                             const pLang : String = 'de') : IWeather5DayForecast; overload;
    function get5DayForecast(const pId : Integer;
                             const pUnits : String = 'metric';
                             const pLang : String = 'de') : IWeather5DayForecast; overload;

    function getStation(const pID : Integer;
                        const pUnits : String = 'metric';
                        const pLang : String = 'de') : IWeatherStation; overload;
    function getStations(const pBBox : TWeatherBBox;
                         const pCluster : String = WEATHER_CLUSTER_NO;
                         const pCnt : Integer = 100;
                         const pUnits : String = 'metric';
                         const pLang : String = 'de') : TWeatherObjectList; overload;
    function getStations(const pLatitude : Double;
                         const pLongitude : Double;
                         const pCnt : Integer = 25;
                         const pUnits : String = 'metric';
                         const pLang : String = 'de') : TWeatherObjectList; overload;

    function transmit(const pData : TWeatherStationData) : Boolean;
  end;

implementation

end.
